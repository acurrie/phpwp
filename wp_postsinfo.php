<?php

// Global variable for table object
$wp_posts = NULL;

//
// Table class for wp_posts
//
class cwp_posts extends cTable {
	var $ID;
	var $post_author;
	var $post_date;
	var $post_date_gmt;
	var $post_content;
	var $post_title;
	var $post_excerpt;
	var $post_status;
	var $comment_status;
	var $ping_status;
	var $post_password;
	var $post_name;
	var $to_ping;
	var $pinged;
	var $post_modified;
	var $post_modified_gmt;
	var $post_content_filtered;
	var $post_parent;
	var $guid;
	var $menu_order;
	var $post_type;
	var $post_mime_type;
	var $comment_count;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'wp_posts';
		$this->TableName = 'wp_posts';
		$this->TableType = 'TABLE';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->DetailView = FALSE; // Allow detail view
		$this->ShowMultipleDetails = FALSE; // Show multiple details
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// ID
		$this->ID = new cField('wp_posts', 'wp_posts', 'x_ID', 'ID', '`ID`', '`ID`', 21, -1, FALSE, '`ID`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->ID->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['ID'] = &$this->ID;

		// post_author
		$this->post_author = new cField('wp_posts', 'wp_posts', 'x_post_author', 'post_author', '`post_author`', '`post_author`', 21, -1, FALSE, '`post_author`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->post_author->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['post_author'] = &$this->post_author;

		// post_date
		$this->post_date = new cField('wp_posts', 'wp_posts', 'x_post_date', 'post_date', '`post_date`', 'DATE_FORMAT(`post_date`, \'%Y/%m/%d\')', 135, 5, FALSE, '`post_date`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->post_date->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateYMD"));
		$this->fields['post_date'] = &$this->post_date;

		// post_date_gmt
		$this->post_date_gmt = new cField('wp_posts', 'wp_posts', 'x_post_date_gmt', 'post_date_gmt', '`post_date_gmt`', 'DATE_FORMAT(`post_date_gmt`, \'%Y/%m/%d\')', 135, 5, FALSE, '`post_date_gmt`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->post_date_gmt->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateYMD"));
		$this->fields['post_date_gmt'] = &$this->post_date_gmt;

		// post_content
		$this->post_content = new cField('wp_posts', 'wp_posts', 'x_post_content', 'post_content', '`post_content`', '`post_content`', 201, -1, FALSE, '`post_content`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['post_content'] = &$this->post_content;

		// post_title
		$this->post_title = new cField('wp_posts', 'wp_posts', 'x_post_title', 'post_title', '`post_title`', '`post_title`', 201, -1, FALSE, '`post_title`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['post_title'] = &$this->post_title;

		// post_excerpt
		$this->post_excerpt = new cField('wp_posts', 'wp_posts', 'x_post_excerpt', 'post_excerpt', '`post_excerpt`', '`post_excerpt`', 201, -1, FALSE, '`post_excerpt`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['post_excerpt'] = &$this->post_excerpt;

		// post_status
		$this->post_status = new cField('wp_posts', 'wp_posts', 'x_post_status', 'post_status', '`post_status`', '`post_status`', 200, -1, FALSE, '`post_status`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['post_status'] = &$this->post_status;

		// comment_status
		$this->comment_status = new cField('wp_posts', 'wp_posts', 'x_comment_status', 'comment_status', '`comment_status`', '`comment_status`', 200, -1, FALSE, '`comment_status`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['comment_status'] = &$this->comment_status;

		// ping_status
		$this->ping_status = new cField('wp_posts', 'wp_posts', 'x_ping_status', 'ping_status', '`ping_status`', '`ping_status`', 200, -1, FALSE, '`ping_status`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['ping_status'] = &$this->ping_status;

		// post_password
		$this->post_password = new cField('wp_posts', 'wp_posts', 'x_post_password', 'post_password', '`post_password`', '`post_password`', 200, -1, FALSE, '`post_password`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['post_password'] = &$this->post_password;

		// post_name
		$this->post_name = new cField('wp_posts', 'wp_posts', 'x_post_name', 'post_name', '`post_name`', '`post_name`', 200, -1, FALSE, '`post_name`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['post_name'] = &$this->post_name;

		// to_ping
		$this->to_ping = new cField('wp_posts', 'wp_posts', 'x_to_ping', 'to_ping', '`to_ping`', '`to_ping`', 201, -1, FALSE, '`to_ping`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['to_ping'] = &$this->to_ping;

		// pinged
		$this->pinged = new cField('wp_posts', 'wp_posts', 'x_pinged', 'pinged', '`pinged`', '`pinged`', 201, -1, FALSE, '`pinged`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['pinged'] = &$this->pinged;

		// post_modified
		$this->post_modified = new cField('wp_posts', 'wp_posts', 'x_post_modified', 'post_modified', '`post_modified`', 'DATE_FORMAT(`post_modified`, \'%Y/%m/%d\')', 135, 5, FALSE, '`post_modified`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->post_modified->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateYMD"));
		$this->fields['post_modified'] = &$this->post_modified;

		// post_modified_gmt
		$this->post_modified_gmt = new cField('wp_posts', 'wp_posts', 'x_post_modified_gmt', 'post_modified_gmt', '`post_modified_gmt`', 'DATE_FORMAT(`post_modified_gmt`, \'%Y/%m/%d\')', 135, 5, FALSE, '`post_modified_gmt`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->post_modified_gmt->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateYMD"));
		$this->fields['post_modified_gmt'] = &$this->post_modified_gmt;

		// post_content_filtered
		$this->post_content_filtered = new cField('wp_posts', 'wp_posts', 'x_post_content_filtered', 'post_content_filtered', '`post_content_filtered`', '`post_content_filtered`', 201, -1, FALSE, '`post_content_filtered`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['post_content_filtered'] = &$this->post_content_filtered;

		// post_parent
		$this->post_parent = new cField('wp_posts', 'wp_posts', 'x_post_parent', 'post_parent', '`post_parent`', '`post_parent`', 21, -1, FALSE, '`post_parent`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->post_parent->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['post_parent'] = &$this->post_parent;

		// guid
		$this->guid = new cField('wp_posts', 'wp_posts', 'x_guid', 'guid', '`guid`', '`guid`', 200, -1, FALSE, '`guid`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['guid'] = &$this->guid;

		// menu_order
		$this->menu_order = new cField('wp_posts', 'wp_posts', 'x_menu_order', 'menu_order', '`menu_order`', '`menu_order`', 3, -1, FALSE, '`menu_order`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->menu_order->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['menu_order'] = &$this->menu_order;

		// post_type
		$this->post_type = new cField('wp_posts', 'wp_posts', 'x_post_type', 'post_type', '`post_type`', '`post_type`', 200, -1, FALSE, '`post_type`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['post_type'] = &$this->post_type;

		// post_mime_type
		$this->post_mime_type = new cField('wp_posts', 'wp_posts', 'x_post_mime_type', 'post_mime_type', '`post_mime_type`', '`post_mime_type`', 200, -1, FALSE, '`post_mime_type`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['post_mime_type'] = &$this->post_mime_type;

		// comment_count
		$this->comment_count = new cField('wp_posts', 'wp_posts', 'x_comment_count', 'comment_count', '`comment_count`', '`comment_count`', 20, -1, FALSE, '`comment_count`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->comment_count->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['comment_count'] = &$this->comment_count;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Table level SQL
	function SqlFrom() { // From
		return "`wp_posts`";
	}

	function SqlSelect() { // Select
		return "SELECT * FROM " . $this->SqlFrom();
	}

	function SqlWhere() { // Where
		$sWhere = "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlGroupBy() { // Group By
		return "";
	}

	function SqlHaving() { // Having
		return "";
	}

	function SqlOrderBy() { // Order By
		return "";
	}

	// Check if Anonymous User is allowed
	function AllowAnonymousUser() {
		switch (@$this->PageID) {
			case "add":
			case "register":
			case "addopt":
				return FALSE;
			case "edit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return FALSE;
			case "delete":
				return FALSE;
			case "view":
				return FALSE;
			case "search":
				return FALSE;
			default:
				return FALSE;
		}
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		$allow = EW_USER_ID_ALLOW;
		switch ($id) {
			case "add":
			case "copy":
			case "gridadd":
			case "register":
			case "addopt":
				return (($allow & 1) == 1);
			case "edit":
			case "gridedit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return (($allow & 4) == 4);
			case "delete":
				return (($allow & 2) == 2);
			case "view":
				return (($allow & 32) == 32);
			case "search":
				return (($allow & 64) == 64);
			default:
				return (($allow & 8) == 8);
		}
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(), $this->SqlGroupBy(),
			$this->SqlHaving(), $this->SqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->SqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		global $conn;
		$cnt = -1;
		if ($this->TableType == 'TABLE' || $this->TableType == 'VIEW') {
			$sSql = "SELECT COUNT(*) FROM" . substr($sSql, 13);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		global $conn;
		$origFilter = $this->CurrentFilter;
		$this->Recordset_Selecting($this->CurrentFilter);
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Update Table
	var $UpdateTable = "`wp_posts`";

	// INSERT statement
	function InsertSQL(&$rs) {
		global $conn;
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		global $conn;
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "") {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "", $rsold = NULL) {
		global $conn;
		return $conn->Execute($this->UpdateSQL($rs, $where));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "") {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if ($rs) {
			if (array_key_exists('ID', $rs))
				ew_AddFilter($where, ew_QuotedName('ID') . '=' . ew_QuotedValue($rs['ID'], $this->ID->FldDataType));
		}
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")
			$sql .= $filter;
		else
			$sql .= "0=1"; // Avoid delete
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "") {
		global $conn;
		return $conn->Execute($this->DeleteSQL($rs, $where));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`ID` = @ID@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->ID->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@ID@", ew_AdjustSql($this->ID->CurrentValue), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "wp_postslist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "wp_postslist.php";
	}

	// View URL
	function GetViewUrl($parm = "") {
		if ($parm <> "")
			return $this->KeyUrl("wp_postsview.php", $this->UrlParm($parm));
		else
			return $this->KeyUrl("wp_postsview.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
	}

	// Add URL
	function GetAddUrl() {
		return "wp_postsadd.php";
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		return $this->KeyUrl("wp_postsedit.php", $this->UrlParm($parm));
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		return $this->KeyUrl("wp_postsadd.php", $this->UrlParm($parm));
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("wp_postsdelete.php", $this->UrlParm());
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->ID->CurrentValue)) {
			$sUrl .= "ID=" . urlencode($this->ID->CurrentValue);
		} else {
			return "javascript:alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET)) {
			$arKeys[] = @$_GET["ID"]; // ID

			//return $arKeys; // Do not return yet, so the values will also be checked by the following code
		}

		// Check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->ID->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {
		global $conn;

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->ID->setDbValue($rs->fields('ID'));
		$this->post_author->setDbValue($rs->fields('post_author'));
		$this->post_date->setDbValue($rs->fields('post_date'));
		$this->post_date_gmt->setDbValue($rs->fields('post_date_gmt'));
		$this->post_content->setDbValue($rs->fields('post_content'));
		$this->post_title->setDbValue($rs->fields('post_title'));
		$this->post_excerpt->setDbValue($rs->fields('post_excerpt'));
		$this->post_status->setDbValue($rs->fields('post_status'));
		$this->comment_status->setDbValue($rs->fields('comment_status'));
		$this->ping_status->setDbValue($rs->fields('ping_status'));
		$this->post_password->setDbValue($rs->fields('post_password'));
		$this->post_name->setDbValue($rs->fields('post_name'));
		$this->to_ping->setDbValue($rs->fields('to_ping'));
		$this->pinged->setDbValue($rs->fields('pinged'));
		$this->post_modified->setDbValue($rs->fields('post_modified'));
		$this->post_modified_gmt->setDbValue($rs->fields('post_modified_gmt'));
		$this->post_content_filtered->setDbValue($rs->fields('post_content_filtered'));
		$this->post_parent->setDbValue($rs->fields('post_parent'));
		$this->guid->setDbValue($rs->fields('guid'));
		$this->menu_order->setDbValue($rs->fields('menu_order'));
		$this->post_type->setDbValue($rs->fields('post_type'));
		$this->post_mime_type->setDbValue($rs->fields('post_mime_type'));
		$this->comment_count->setDbValue($rs->fields('comment_count'));
	}

	// Render list row values
	function RenderListRow() {
		global $conn, $Security;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// ID
		// post_author
		// post_date
		// post_date_gmt
		// post_content
		// post_title
		// post_excerpt
		// post_status
		// comment_status
		// ping_status
		// post_password
		// post_name
		// to_ping
		// pinged
		// post_modified
		// post_modified_gmt
		// post_content_filtered
		// post_parent
		// guid
		// menu_order
		// post_type
		// post_mime_type
		// comment_count
		// ID

		$this->ID->ViewValue = $this->ID->CurrentValue;
		$this->ID->ViewCustomAttributes = "";

		// post_author
		$this->post_author->ViewValue = $this->post_author->CurrentValue;
		$this->post_author->ViewCustomAttributes = "";

		// post_date
		$this->post_date->ViewValue = $this->post_date->CurrentValue;
		$this->post_date->ViewValue = ew_FormatDateTime($this->post_date->ViewValue, 5);
		$this->post_date->ViewCustomAttributes = "";

		// post_date_gmt
		$this->post_date_gmt->ViewValue = $this->post_date_gmt->CurrentValue;
		$this->post_date_gmt->ViewValue = ew_FormatDateTime($this->post_date_gmt->ViewValue, 5);
		$this->post_date_gmt->ViewCustomAttributes = "";

		// post_content
		$this->post_content->ViewValue = $this->post_content->CurrentValue;
		$this->post_content->ViewCustomAttributes = "";

		// post_title
		$this->post_title->ViewValue = $this->post_title->CurrentValue;
		$this->post_title->ViewCustomAttributes = "";

		// post_excerpt
		$this->post_excerpt->ViewValue = $this->post_excerpt->CurrentValue;
		$this->post_excerpt->ViewCustomAttributes = "";

		// post_status
		$this->post_status->ViewValue = $this->post_status->CurrentValue;
		$this->post_status->ViewCustomAttributes = "";

		// comment_status
		$this->comment_status->ViewValue = $this->comment_status->CurrentValue;
		$this->comment_status->ViewCustomAttributes = "";

		// ping_status
		$this->ping_status->ViewValue = $this->ping_status->CurrentValue;
		$this->ping_status->ViewCustomAttributes = "";

		// post_password
		$this->post_password->ViewValue = $this->post_password->CurrentValue;
		$this->post_password->ViewCustomAttributes = "";

		// post_name
		$this->post_name->ViewValue = $this->post_name->CurrentValue;
		$this->post_name->ViewCustomAttributes = "";

		// to_ping
		$this->to_ping->ViewValue = $this->to_ping->CurrentValue;
		$this->to_ping->ViewCustomAttributes = "";

		// pinged
		$this->pinged->ViewValue = $this->pinged->CurrentValue;
		$this->pinged->ViewCustomAttributes = "";

		// post_modified
		$this->post_modified->ViewValue = $this->post_modified->CurrentValue;
		$this->post_modified->ViewValue = ew_FormatDateTime($this->post_modified->ViewValue, 5);
		$this->post_modified->ViewCustomAttributes = "";

		// post_modified_gmt
		$this->post_modified_gmt->ViewValue = $this->post_modified_gmt->CurrentValue;
		$this->post_modified_gmt->ViewValue = ew_FormatDateTime($this->post_modified_gmt->ViewValue, 5);
		$this->post_modified_gmt->ViewCustomAttributes = "";

		// post_content_filtered
		$this->post_content_filtered->ViewValue = $this->post_content_filtered->CurrentValue;
		$this->post_content_filtered->ViewCustomAttributes = "";

		// post_parent
		$this->post_parent->ViewValue = $this->post_parent->CurrentValue;
		$this->post_parent->ViewCustomAttributes = "";

		// guid
		$this->guid->ViewValue = $this->guid->CurrentValue;
		$this->guid->ViewCustomAttributes = "";

		// menu_order
		$this->menu_order->ViewValue = $this->menu_order->CurrentValue;
		$this->menu_order->ViewCustomAttributes = "";

		// post_type
		$this->post_type->ViewValue = $this->post_type->CurrentValue;
		$this->post_type->ViewCustomAttributes = "";

		// post_mime_type
		$this->post_mime_type->ViewValue = $this->post_mime_type->CurrentValue;
		$this->post_mime_type->ViewCustomAttributes = "";

		// comment_count
		$this->comment_count->ViewValue = $this->comment_count->CurrentValue;
		$this->comment_count->ViewCustomAttributes = "";

		// ID
		$this->ID->LinkCustomAttributes = "";
		$this->ID->HrefValue = "";
		$this->ID->TooltipValue = "";

		// post_author
		$this->post_author->LinkCustomAttributes = "";
		$this->post_author->HrefValue = "";
		$this->post_author->TooltipValue = "";

		// post_date
		$this->post_date->LinkCustomAttributes = "";
		$this->post_date->HrefValue = "";
		$this->post_date->TooltipValue = "";

		// post_date_gmt
		$this->post_date_gmt->LinkCustomAttributes = "";
		$this->post_date_gmt->HrefValue = "";
		$this->post_date_gmt->TooltipValue = "";

		// post_content
		$this->post_content->LinkCustomAttributes = "";
		$this->post_content->HrefValue = "";
		$this->post_content->TooltipValue = "";

		// post_title
		$this->post_title->LinkCustomAttributes = "";
		$this->post_title->HrefValue = "";
		$this->post_title->TooltipValue = "";

		// post_excerpt
		$this->post_excerpt->LinkCustomAttributes = "";
		$this->post_excerpt->HrefValue = "";
		$this->post_excerpt->TooltipValue = "";

		// post_status
		$this->post_status->LinkCustomAttributes = "";
		$this->post_status->HrefValue = "";
		$this->post_status->TooltipValue = "";

		// comment_status
		$this->comment_status->LinkCustomAttributes = "";
		$this->comment_status->HrefValue = "";
		$this->comment_status->TooltipValue = "";

		// ping_status
		$this->ping_status->LinkCustomAttributes = "";
		$this->ping_status->HrefValue = "";
		$this->ping_status->TooltipValue = "";

		// post_password
		$this->post_password->LinkCustomAttributes = "";
		$this->post_password->HrefValue = "";
		$this->post_password->TooltipValue = "";

		// post_name
		$this->post_name->LinkCustomAttributes = "";
		$this->post_name->HrefValue = "";
		$this->post_name->TooltipValue = "";

		// to_ping
		$this->to_ping->LinkCustomAttributes = "";
		$this->to_ping->HrefValue = "";
		$this->to_ping->TooltipValue = "";

		// pinged
		$this->pinged->LinkCustomAttributes = "";
		$this->pinged->HrefValue = "";
		$this->pinged->TooltipValue = "";

		// post_modified
		$this->post_modified->LinkCustomAttributes = "";
		$this->post_modified->HrefValue = "";
		$this->post_modified->TooltipValue = "";

		// post_modified_gmt
		$this->post_modified_gmt->LinkCustomAttributes = "";
		$this->post_modified_gmt->HrefValue = "";
		$this->post_modified_gmt->TooltipValue = "";

		// post_content_filtered
		$this->post_content_filtered->LinkCustomAttributes = "";
		$this->post_content_filtered->HrefValue = "";
		$this->post_content_filtered->TooltipValue = "";

		// post_parent
		$this->post_parent->LinkCustomAttributes = "";
		$this->post_parent->HrefValue = "";
		$this->post_parent->TooltipValue = "";

		// guid
		$this->guid->LinkCustomAttributes = "";
		$this->guid->HrefValue = "";
		$this->guid->TooltipValue = "";

		// menu_order
		$this->menu_order->LinkCustomAttributes = "";
		$this->menu_order->HrefValue = "";
		$this->menu_order->TooltipValue = "";

		// post_type
		$this->post_type->LinkCustomAttributes = "";
		$this->post_type->HrefValue = "";
		$this->post_type->TooltipValue = "";

		// post_mime_type
		$this->post_mime_type->LinkCustomAttributes = "";
		$this->post_mime_type->HrefValue = "";
		$this->post_mime_type->TooltipValue = "";

		// comment_count
		$this->comment_count->LinkCustomAttributes = "";
		$this->comment_count->HrefValue = "";
		$this->comment_count->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {
	}

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;

		// Write header
		$Doc->ExportTableHeader();
		if ($Doc->Horizontal) { // Horizontal format, write header
			$Doc->BeginExportRow();
			if ($ExportPageType == "view") {
				if ($this->ID->Exportable) $Doc->ExportCaption($this->ID);
				if ($this->post_author->Exportable) $Doc->ExportCaption($this->post_author);
				if ($this->post_date->Exportable) $Doc->ExportCaption($this->post_date);
				if ($this->post_date_gmt->Exportable) $Doc->ExportCaption($this->post_date_gmt);
				if ($this->post_content->Exportable) $Doc->ExportCaption($this->post_content);
				if ($this->post_title->Exportable) $Doc->ExportCaption($this->post_title);
				if ($this->post_excerpt->Exportable) $Doc->ExportCaption($this->post_excerpt);
				if ($this->post_status->Exportable) $Doc->ExportCaption($this->post_status);
				if ($this->comment_status->Exportable) $Doc->ExportCaption($this->comment_status);
				if ($this->ping_status->Exportable) $Doc->ExportCaption($this->ping_status);
				if ($this->post_password->Exportable) $Doc->ExportCaption($this->post_password);
				if ($this->post_name->Exportable) $Doc->ExportCaption($this->post_name);
				if ($this->to_ping->Exportable) $Doc->ExportCaption($this->to_ping);
				if ($this->pinged->Exportable) $Doc->ExportCaption($this->pinged);
				if ($this->post_modified->Exportable) $Doc->ExportCaption($this->post_modified);
				if ($this->post_modified_gmt->Exportable) $Doc->ExportCaption($this->post_modified_gmt);
				if ($this->post_content_filtered->Exportable) $Doc->ExportCaption($this->post_content_filtered);
				if ($this->post_parent->Exportable) $Doc->ExportCaption($this->post_parent);
				if ($this->guid->Exportable) $Doc->ExportCaption($this->guid);
				if ($this->menu_order->Exportable) $Doc->ExportCaption($this->menu_order);
				if ($this->post_type->Exportable) $Doc->ExportCaption($this->post_type);
				if ($this->post_mime_type->Exportable) $Doc->ExportCaption($this->post_mime_type);
				if ($this->comment_count->Exportable) $Doc->ExportCaption($this->comment_count);
			} else {
				if ($this->ID->Exportable) $Doc->ExportCaption($this->ID);
				if ($this->post_author->Exportable) $Doc->ExportCaption($this->post_author);
				if ($this->post_date->Exportable) $Doc->ExportCaption($this->post_date);
				if ($this->post_date_gmt->Exportable) $Doc->ExportCaption($this->post_date_gmt);
				if ($this->post_status->Exportable) $Doc->ExportCaption($this->post_status);
				if ($this->comment_status->Exportable) $Doc->ExportCaption($this->comment_status);
				if ($this->ping_status->Exportable) $Doc->ExportCaption($this->ping_status);
				if ($this->post_password->Exportable) $Doc->ExportCaption($this->post_password);
				if ($this->post_name->Exportable) $Doc->ExportCaption($this->post_name);
				if ($this->post_modified->Exportable) $Doc->ExportCaption($this->post_modified);
				if ($this->post_modified_gmt->Exportable) $Doc->ExportCaption($this->post_modified_gmt);
				if ($this->post_parent->Exportable) $Doc->ExportCaption($this->post_parent);
				if ($this->guid->Exportable) $Doc->ExportCaption($this->guid);
				if ($this->menu_order->Exportable) $Doc->ExportCaption($this->menu_order);
				if ($this->post_type->Exportable) $Doc->ExportCaption($this->post_type);
				if ($this->post_mime_type->Exportable) $Doc->ExportCaption($this->post_mime_type);
				if ($this->comment_count->Exportable) $Doc->ExportCaption($this->comment_count);
			}
			$Doc->EndExportRow();
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
				if ($ExportPageType == "view") {
					if ($this->ID->Exportable) $Doc->ExportField($this->ID);
					if ($this->post_author->Exportable) $Doc->ExportField($this->post_author);
					if ($this->post_date->Exportable) $Doc->ExportField($this->post_date);
					if ($this->post_date_gmt->Exportable) $Doc->ExportField($this->post_date_gmt);
					if ($this->post_content->Exportable) $Doc->ExportField($this->post_content);
					if ($this->post_title->Exportable) $Doc->ExportField($this->post_title);
					if ($this->post_excerpt->Exportable) $Doc->ExportField($this->post_excerpt);
					if ($this->post_status->Exportable) $Doc->ExportField($this->post_status);
					if ($this->comment_status->Exportable) $Doc->ExportField($this->comment_status);
					if ($this->ping_status->Exportable) $Doc->ExportField($this->ping_status);
					if ($this->post_password->Exportable) $Doc->ExportField($this->post_password);
					if ($this->post_name->Exportable) $Doc->ExportField($this->post_name);
					if ($this->to_ping->Exportable) $Doc->ExportField($this->to_ping);
					if ($this->pinged->Exportable) $Doc->ExportField($this->pinged);
					if ($this->post_modified->Exportable) $Doc->ExportField($this->post_modified);
					if ($this->post_modified_gmt->Exportable) $Doc->ExportField($this->post_modified_gmt);
					if ($this->post_content_filtered->Exportable) $Doc->ExportField($this->post_content_filtered);
					if ($this->post_parent->Exportable) $Doc->ExportField($this->post_parent);
					if ($this->guid->Exportable) $Doc->ExportField($this->guid);
					if ($this->menu_order->Exportable) $Doc->ExportField($this->menu_order);
					if ($this->post_type->Exportable) $Doc->ExportField($this->post_type);
					if ($this->post_mime_type->Exportable) $Doc->ExportField($this->post_mime_type);
					if ($this->comment_count->Exportable) $Doc->ExportField($this->comment_count);
				} else {
					if ($this->ID->Exportable) $Doc->ExportField($this->ID);
					if ($this->post_author->Exportable) $Doc->ExportField($this->post_author);
					if ($this->post_date->Exportable) $Doc->ExportField($this->post_date);
					if ($this->post_date_gmt->Exportable) $Doc->ExportField($this->post_date_gmt);
					if ($this->post_status->Exportable) $Doc->ExportField($this->post_status);
					if ($this->comment_status->Exportable) $Doc->ExportField($this->comment_status);
					if ($this->ping_status->Exportable) $Doc->ExportField($this->ping_status);
					if ($this->post_password->Exportable) $Doc->ExportField($this->post_password);
					if ($this->post_name->Exportable) $Doc->ExportField($this->post_name);
					if ($this->post_modified->Exportable) $Doc->ExportField($this->post_modified);
					if ($this->post_modified_gmt->Exportable) $Doc->ExportField($this->post_modified_gmt);
					if ($this->post_parent->Exportable) $Doc->ExportField($this->post_parent);
					if ($this->guid->Exportable) $Doc->ExportField($this->guid);
					if ($this->menu_order->Exportable) $Doc->ExportField($this->menu_order);
					if ($this->post_type->Exportable) $Doc->ExportField($this->post_type);
					if ($this->post_mime_type->Exportable) $Doc->ExportField($this->post_mime_type);
					if ($this->comment_count->Exportable) $Doc->ExportField($this->comment_count);
				}
				$Doc->EndExportRow();
			}
			$Recordset->MoveNext();
		}
		$Doc->ExportTableFooter();
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Lookup Selecting event
	function Lookup_Selecting($fld, &$filter) {

		// Enter your code here
	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
