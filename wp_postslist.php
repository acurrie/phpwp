<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg10.php" ?>
<?php include_once "ewmysql10.php" ?>
<?php include_once "phpfn10.php" ?>
<?php include_once "wp_postsinfo.php" ?>
<?php include_once "userfn10.php" ?>
<?php

//
// Page class
//

$wp_posts_list = NULL; // Initialize page object first

class cwp_posts_list extends cwp_posts {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{9787E536-E7C1-412B-A7C2-084F8BF0B798}";

	// Table name
	var $TableName = 'wp_posts';

	// Page object name
	var $PageObjName = 'wp_posts_list';

	// Grid form hidden field names
	var $FormName = 'fwp_postslist';
	var $FormActionName = 'k_action';
	var $FormKeyName = 'k_key';
	var $FormOldKeyName = 'k_oldkey';
	var $FormBlankRowName = 'k_blankrow';
	var $FormKeyCountName = 'key_count';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-error ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<table class=\"ewStdTable\"><tr><td><div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div></td></tr></table>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (wp_posts)
		if (!isset($GLOBALS["wp_posts"])) {
			$GLOBALS["wp_posts"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["wp_posts"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "wp_postsadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "wp_postsdelete.php";
		$this->MultiUpdateUrl = "wp_postsupdate.php";

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'wp_posts', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "span";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['addedit'] = new cListOptions();
		$this->OtherOptions['addedit']->Tag = "span";
		$this->OtherOptions['addedit']->TagClassName = "ewAddEditOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "span";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "span";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up curent action

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();
		$this->ID->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();

		// Setup other options
		$this->SetupOtherOptions();

		// Set "checkbox" visible
		if (count($this->CustomActions) > 0)
			$this->ListOptions->Items["checkbox"]->Visible = TRUE;
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $RestoreSearch = FALSE;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Process custom action first
			$this->ProcessCustomAction();

			// Handle reset command
			$this->ResetCmd();

			// Set up Breadcrumb
			$this->SetupBreadcrumb();

			// Hide list options
			if ($this->Export <> "") {
				$this->ListOptions->HideAllOptions(array("sequence"));
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			} elseif ($this->CurrentAction == "gridadd" || $this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ListOptions->UseDropDownButton = FALSE; // Disable drop down button
				$this->ListOptions->UseButtonGroup = FALSE; // Disable button group
			}

			// Hide export options
			if ($this->Export <> "" || $this->CurrentAction <> "")
				$this->ExportOptions->HideAllOptions();

			// Hide other options
			if ($this->Export <> "") {
				foreach ($this->OtherOptions as &$option)
					$option->HideAllOptions();
			}

			// Get basic search values
			$this->LoadBasicSearchValues();

			// Restore search parms from Session if not searching / reset
			if ($this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall" && $this->CheckSearchParms())
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get basic search criteria
			if ($gsSearchError == "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load basic search from default
			$this->BasicSearch->LoadDefault();
			if ($this->BasicSearch->Keyword != "")
				$sSrchBasic = $this->BasicSearchWhere();
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search" && !$this->RestoreSearch) {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue("k_key"));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // Next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue("k_key"));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->ID->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->ID->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Return basic search SQL
	function BasicSearchSQL($Keyword) {
		$sKeyword = ew_AdjustSql($Keyword);
		$sWhere = "";
		$this->BuildBasicSearchSQL($sWhere, $this->post_content, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->post_title, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->post_excerpt, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->post_status, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->comment_status, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->ping_status, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->post_password, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->post_name, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->to_ping, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->pinged, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->post_content_filtered, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->guid, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->post_type, $Keyword);
		$this->BuildBasicSearchSQL($sWhere, $this->post_mime_type, $Keyword);
		return $sWhere;
	}

	// Build basic search SQL
	function BuildBasicSearchSql(&$Where, &$Fld, $Keyword) {
		if ($Keyword == EW_NULL_VALUE) {
			$sWrk = $Fld->FldExpression . " IS NULL";
		} elseif ($Keyword == EW_NOT_NULL_VALUE) {
			$sWrk = $Fld->FldExpression . " IS NOT NULL";
		} else {
			$sFldExpression = ($Fld->FldVirtualExpression <> $Fld->FldExpression) ? $Fld->FldVirtualExpression : $Fld->FldBasicSearchExpression;
			$sWrk = $sFldExpression . ew_Like(ew_QuotedValue("%" . $Keyword . "%", EW_DATATYPE_STRING));
		}
		if ($Where <> "") $Where .= " OR ";
		$Where .= $sWrk;
	}

	// Return basic search WHERE clause based on search keyword and type
	function BasicSearchWhere() {
		global $Security;
		$sSearchStr = "";
		$sSearchKeyword = $this->BasicSearch->Keyword;
		$sSearchType = $this->BasicSearch->Type;
		if ($sSearchKeyword <> "") {
			$sSearch = trim($sSearchKeyword);
			if ($sSearchType <> "=") {
				while (strpos($sSearch, "  ") !== FALSE)
					$sSearch = str_replace("  ", " ", $sSearch);
				$arKeyword = explode(" ", trim($sSearch));
				foreach ($arKeyword as $sKeyword) {
					if ($sSearchStr <> "") $sSearchStr .= " " . $sSearchType . " ";
					$sSearchStr .= "(" . $this->BasicSearchSQL($sKeyword) . ")";
				}
			} else {
				$sSearchStr = $this->BasicSearchSQL($sSearch);
			}
			$this->Command = "search";
		}
		if ($this->Command == "search") {
			$this->BasicSearch->setKeyword($sSearchKeyword);
			$this->BasicSearch->setType($sSearchType);
		}
		return $sSearchStr;
	}

	// Check if search parm exists
	function CheckSearchParms() {

		// Check basic search
		if ($this->BasicSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear basic search parameters
		$this->ResetBasicSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all basic search parameters
	function ResetBasicSearchParms() {
		$this->BasicSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {
		$this->RestoreSearch = TRUE;

		// Restore basic search values
		$this->BasicSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->ID); // ID
			$this->UpdateSort($this->post_author); // post_author
			$this->UpdateSort($this->post_date); // post_date
			$this->UpdateSort($this->post_date_gmt); // post_date_gmt
			$this->UpdateSort($this->post_status); // post_status
			$this->UpdateSort($this->comment_status); // comment_status
			$this->UpdateSort($this->ping_status); // ping_status
			$this->UpdateSort($this->post_password); // post_password
			$this->UpdateSort($this->post_name); // post_name
			$this->UpdateSort($this->post_modified); // post_modified
			$this->UpdateSort($this->post_modified_gmt); // post_modified_gmt
			$this->UpdateSort($this->post_parent); // post_parent
			$this->UpdateSort($this->guid); // guid
			$this->UpdateSort($this->menu_order); // menu_order
			$this->UpdateSort($this->post_type); // post_type
			$this->UpdateSort($this->post_mime_type); // post_mime_type
			$this->UpdateSort($this->comment_count); // comment_count
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->SqlOrderBy() <> "") {
				$sOrderBy = $this->SqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// - cmd=reset (Reset search parameters)
	// - cmd=resetall (Reset search and master/detail parameters)
	// - cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->ID->setSort("");
				$this->post_author->setSort("");
				$this->post_date->setSort("");
				$this->post_date_gmt->setSort("");
				$this->post_status->setSort("");
				$this->comment_status->setSort("");
				$this->ping_status->setSort("");
				$this->post_password->setSort("");
				$this->post_name->setSort("");
				$this->post_modified->setSort("");
				$this->post_modified_gmt->setSort("");
				$this->post_parent->setSort("");
				$this->guid->setSort("");
				$this->menu_order->setSort("");
				$this->post_type->setSort("");
				$this->post_mime_type->setSort("");
				$this->comment_count->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// Add group option item
		$item = &$this->ListOptions->Add($this->ListOptions->GroupOptionName);
		$item->Body = "";
		$item->OnLeft = FALSE;
		$item->Visible = FALSE;

		// "view"
		$item = &$this->ListOptions->Add("view");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = TRUE;
		$item->OnLeft = FALSE;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = TRUE;
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = TRUE;
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = TRUE;
		$item->OnLeft = FALSE;

		// "checkbox"
		$item = &$this->ListOptions->Add("checkbox");
		$item->Visible = FALSE;
		$item->OnLeft = FALSE;
		$item->Header = "<label class=\"checkbox\"><input type=\"checkbox\" name=\"key\" id=\"key\" onclick=\"ew_SelectAllKey(this);\"></label>";
		$item->ShowInDropDown = FALSE;
		$item->ShowInButtonGroup = FALSE;

		// Drop down button for ListOptions
		$this->ListOptions->UseDropDownButton = FALSE;
		$this->ListOptions->DropDownButtonPhrase = $Language->Phrase("ButtonListOptions");
		$this->ListOptions->UseButtonGroup = FALSE;
		$this->ListOptions->ButtonClass = "btn-small"; // Class for button group

		// Call ListOptions_Load event
		$this->ListOptions_Load();
		$item = &$this->ListOptions->GetItem($this->ListOptions->GroupOptionName);
		$item->Visible = $this->ListOptions->GroupOptionVisible();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// "view"
		$oListOpt = &$this->ListOptions->Items["view"];
		if (TRUE)
			$oListOpt->Body = "<a class=\"ewRowLink ewView\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("ViewLink")) . "\" href=\"" . ew_HtmlEncode($this->ViewUrl) . "\">" . $Language->Phrase("ViewLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if (TRUE) {
			$oListOpt->Body = "<a class=\"ewRowLink ewEdit\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("EditLink")) . "\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("EditLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if (TRUE) {
			$oListOpt->Body = "<a class=\"ewRowLink ewCopy\" data-caption=\"" . ew_HtmlTitle($Language->Phrase("CopyLink")) . "\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("CopyLink") . "</a>";
		} else {
			$oListOpt->Body = "";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if (TRUE)
			$oListOpt->Body = "<a class=\"ewRowLink ewDelete\"" . "" . " data-caption=\"" . ew_HtmlTitle($Language->Phrase("DeleteLink")) . "\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		else
			$oListOpt->Body = "";

		// "checkbox"
		$oListOpt = &$this->ListOptions->Items["checkbox"];
		$oListOpt->Body = "<label class=\"checkbox\"><input type=\"checkbox\" name=\"key_m[]\" value=\"" . ew_HtmlEncode($this->ID->CurrentValue) . "\" onclick='ew_ClickMultiCheckbox(event, this);'></label>";
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = $options["addedit"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAddEdit ewAdd\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("AddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "");
		$option = $options["action"];

		// Set up options default
		foreach ($options as &$option) {
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$option->ButtonClass = "btn-small"; // Class for button group
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["addedit"]->DropDownButtonPhrase = $Language->Phrase("ButtonAddEdit");
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
	}

	// Render other options
	function RenderOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
			$option = &$options["action"];
			foreach ($this->CustomActions as $action => $name) {

				// Add custom action
				$item = &$option->Add("custom_" . $action);
				$item->Body = "<a class=\"ewAction ewCustomAction\" href=\"\" onclick=\"ew_SubmitSelected(document.fwp_postslist, '" . ew_CurrentUrl() . "', null, '" . $action . "');return false;\">" . $name . "</a>";
			}

			// Hide grid edit, multi-delete and multi-update
			if ($this->TotalRecs <= 0) {
				$option = &$options["addedit"];
				$item = &$option->GetItem("gridedit");
				if ($item) $item->Visible = FALSE;
				$option = &$options["action"];
				$item = &$option->GetItem("multidelete");
				if ($item) $item->Visible = FALSE;
				$item = &$option->GetItem("multiupdate");
				if ($item) $item->Visible = FALSE;
			}
	}

	// Process custom action
	function ProcessCustomAction() {
		global $conn, $Language, $Security;
		$sFilter = $this->GetKeyFilter();
		$UserAction = @$_POST["useraction"];
		if ($sFilter <> "" && $UserAction <> "") {
			$this->CurrentFilter = $sFilter;
			$sSql = $this->SQL();
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$rs = $conn->Execute($sSql);
			$conn->raiseErrorFn = '';
			$rsuser = ($rs) ? $rs->GetRows() : array();
			if ($rs)
				$rs->Close();

			// Call row custom action event
			if (count($rsuser) > 0) {
				$conn->BeginTrans();
				foreach ($rsuser as $row) {
					$Processed = $this->Row_CustomAction($UserAction, $row);
					if (!$Processed) break;
				}
				if ($Processed) {
					$conn->CommitTrans(); // Commit the changes
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage(str_replace('%s', $UserAction, $Language->Phrase("CustomActionCompleted"))); // Set up success message
				} else {
					$conn->RollbackTrans(); // Rollback changes

					// Set up error message
					if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

						// Use the message, do nothing
					} elseif ($this->CancelMessage <> "") {
						$this->setFailureMessage($this->CancelMessage);
						$this->CancelMessage = "";
					} else {
						$this->setFailureMessage(str_replace('%s', $UserAction, $Language->Phrase("CustomActionCancelled")));
					}
				}
			}
		}
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load basic search values
	function LoadBasicSearchValues() {
		$this->BasicSearch->Keyword = @$_GET[EW_TABLE_BASIC_SEARCH];
		if ($this->BasicSearch->Keyword <> "") $this->Command = "search";
		$this->BasicSearch->Type = @$_GET[EW_TABLE_BASIC_SEARCH_TYPE];
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Call Recordset Selecting event
		$this->Recordset_Selecting($this->CurrentFilter);

		// Load List page SQL
		$sSql = $this->SelectSQL();
		if ($offset > -1 && $rowcnt > -1)
			$sSql .= " LIMIT $rowcnt OFFSET $offset";

		// Load recordset
		$rs = ew_LoadRecordset($sSql);

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->ID->setDbValue($rs->fields('ID'));
		$this->post_author->setDbValue($rs->fields('post_author'));
		$this->post_date->setDbValue($rs->fields('post_date'));
		$this->post_date_gmt->setDbValue($rs->fields('post_date_gmt'));
		$this->post_content->setDbValue($rs->fields('post_content'));
		$this->post_title->setDbValue($rs->fields('post_title'));
		$this->post_excerpt->setDbValue($rs->fields('post_excerpt'));
		$this->post_status->setDbValue($rs->fields('post_status'));
		$this->comment_status->setDbValue($rs->fields('comment_status'));
		$this->ping_status->setDbValue($rs->fields('ping_status'));
		$this->post_password->setDbValue($rs->fields('post_password'));
		$this->post_name->setDbValue($rs->fields('post_name'));
		$this->to_ping->setDbValue($rs->fields('to_ping'));
		$this->pinged->setDbValue($rs->fields('pinged'));
		$this->post_modified->setDbValue($rs->fields('post_modified'));
		$this->post_modified_gmt->setDbValue($rs->fields('post_modified_gmt'));
		$this->post_content_filtered->setDbValue($rs->fields('post_content_filtered'));
		$this->post_parent->setDbValue($rs->fields('post_parent'));
		$this->guid->setDbValue($rs->fields('guid'));
		$this->menu_order->setDbValue($rs->fields('menu_order'));
		$this->post_type->setDbValue($rs->fields('post_type'));
		$this->post_mime_type->setDbValue($rs->fields('post_mime_type'));
		$this->comment_count->setDbValue($rs->fields('comment_count'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->ID->DbValue = $row['ID'];
		$this->post_author->DbValue = $row['post_author'];
		$this->post_date->DbValue = $row['post_date'];
		$this->post_date_gmt->DbValue = $row['post_date_gmt'];
		$this->post_content->DbValue = $row['post_content'];
		$this->post_title->DbValue = $row['post_title'];
		$this->post_excerpt->DbValue = $row['post_excerpt'];
		$this->post_status->DbValue = $row['post_status'];
		$this->comment_status->DbValue = $row['comment_status'];
		$this->ping_status->DbValue = $row['ping_status'];
		$this->post_password->DbValue = $row['post_password'];
		$this->post_name->DbValue = $row['post_name'];
		$this->to_ping->DbValue = $row['to_ping'];
		$this->pinged->DbValue = $row['pinged'];
		$this->post_modified->DbValue = $row['post_modified'];
		$this->post_modified_gmt->DbValue = $row['post_modified_gmt'];
		$this->post_content_filtered->DbValue = $row['post_content_filtered'];
		$this->post_parent->DbValue = $row['post_parent'];
		$this->guid->DbValue = $row['guid'];
		$this->menu_order->DbValue = $row['menu_order'];
		$this->post_type->DbValue = $row['post_type'];
		$this->post_mime_type->DbValue = $row['post_mime_type'];
		$this->comment_count->DbValue = $row['comment_count'];
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("ID")) <> "")
			$this->ID->CurrentValue = $this->getKey("ID"); // ID
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$this->OldRecordset = ew_LoadRecordset($sSql);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// ID
		// post_author
		// post_date
		// post_date_gmt
		// post_content
		// post_title
		// post_excerpt
		// post_status
		// comment_status
		// ping_status
		// post_password
		// post_name
		// to_ping
		// pinged
		// post_modified
		// post_modified_gmt
		// post_content_filtered
		// post_parent
		// guid
		// menu_order
		// post_type
		// post_mime_type
		// comment_count

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// ID
			$this->ID->ViewValue = $this->ID->CurrentValue;
			$this->ID->ViewCustomAttributes = "";

			// post_author
			$this->post_author->ViewValue = $this->post_author->CurrentValue;
			$this->post_author->ViewCustomAttributes = "";

			// post_date
			$this->post_date->ViewValue = $this->post_date->CurrentValue;
			$this->post_date->ViewValue = ew_FormatDateTime($this->post_date->ViewValue, 5);
			$this->post_date->ViewCustomAttributes = "";

			// post_date_gmt
			$this->post_date_gmt->ViewValue = $this->post_date_gmt->CurrentValue;
			$this->post_date_gmt->ViewValue = ew_FormatDateTime($this->post_date_gmt->ViewValue, 5);
			$this->post_date_gmt->ViewCustomAttributes = "";

			// post_status
			$this->post_status->ViewValue = $this->post_status->CurrentValue;
			$this->post_status->ViewCustomAttributes = "";

			// comment_status
			$this->comment_status->ViewValue = $this->comment_status->CurrentValue;
			$this->comment_status->ViewCustomAttributes = "";

			// ping_status
			$this->ping_status->ViewValue = $this->ping_status->CurrentValue;
			$this->ping_status->ViewCustomAttributes = "";

			// post_password
			$this->post_password->ViewValue = $this->post_password->CurrentValue;
			$this->post_password->ViewCustomAttributes = "";

			// post_name
			$this->post_name->ViewValue = $this->post_name->CurrentValue;
			$this->post_name->ViewCustomAttributes = "";

			// post_modified
			$this->post_modified->ViewValue = $this->post_modified->CurrentValue;
			$this->post_modified->ViewValue = ew_FormatDateTime($this->post_modified->ViewValue, 5);
			$this->post_modified->ViewCustomAttributes = "";

			// post_modified_gmt
			$this->post_modified_gmt->ViewValue = $this->post_modified_gmt->CurrentValue;
			$this->post_modified_gmt->ViewValue = ew_FormatDateTime($this->post_modified_gmt->ViewValue, 5);
			$this->post_modified_gmt->ViewCustomAttributes = "";

			// post_parent
			$this->post_parent->ViewValue = $this->post_parent->CurrentValue;
			$this->post_parent->ViewCustomAttributes = "";

			// guid
			$this->guid->ViewValue = $this->guid->CurrentValue;
			$this->guid->ViewCustomAttributes = "";

			// menu_order
			$this->menu_order->ViewValue = $this->menu_order->CurrentValue;
			$this->menu_order->ViewCustomAttributes = "";

			// post_type
			$this->post_type->ViewValue = $this->post_type->CurrentValue;
			$this->post_type->ViewCustomAttributes = "";

			// post_mime_type
			$this->post_mime_type->ViewValue = $this->post_mime_type->CurrentValue;
			$this->post_mime_type->ViewCustomAttributes = "";

			// comment_count
			$this->comment_count->ViewValue = $this->comment_count->CurrentValue;
			$this->comment_count->ViewCustomAttributes = "";

			// ID
			$this->ID->LinkCustomAttributes = "";
			$this->ID->HrefValue = "";
			$this->ID->TooltipValue = "";

			// post_author
			$this->post_author->LinkCustomAttributes = "";
			$this->post_author->HrefValue = "";
			$this->post_author->TooltipValue = "";

			// post_date
			$this->post_date->LinkCustomAttributes = "";
			$this->post_date->HrefValue = "";
			$this->post_date->TooltipValue = "";

			// post_date_gmt
			$this->post_date_gmt->LinkCustomAttributes = "";
			$this->post_date_gmt->HrefValue = "";
			$this->post_date_gmt->TooltipValue = "";

			// post_status
			$this->post_status->LinkCustomAttributes = "";
			$this->post_status->HrefValue = "";
			$this->post_status->TooltipValue = "";

			// comment_status
			$this->comment_status->LinkCustomAttributes = "";
			$this->comment_status->HrefValue = "";
			$this->comment_status->TooltipValue = "";

			// ping_status
			$this->ping_status->LinkCustomAttributes = "";
			$this->ping_status->HrefValue = "";
			$this->ping_status->TooltipValue = "";

			// post_password
			$this->post_password->LinkCustomAttributes = "";
			$this->post_password->HrefValue = "";
			$this->post_password->TooltipValue = "";

			// post_name
			$this->post_name->LinkCustomAttributes = "";
			$this->post_name->HrefValue = "";
			$this->post_name->TooltipValue = "";

			// post_modified
			$this->post_modified->LinkCustomAttributes = "";
			$this->post_modified->HrefValue = "";
			$this->post_modified->TooltipValue = "";

			// post_modified_gmt
			$this->post_modified_gmt->LinkCustomAttributes = "";
			$this->post_modified_gmt->HrefValue = "";
			$this->post_modified_gmt->TooltipValue = "";

			// post_parent
			$this->post_parent->LinkCustomAttributes = "";
			$this->post_parent->HrefValue = "";
			$this->post_parent->TooltipValue = "";

			// guid
			$this->guid->LinkCustomAttributes = "";
			$this->guid->HrefValue = "";
			$this->guid->TooltipValue = "";

			// menu_order
			$this->menu_order->LinkCustomAttributes = "";
			$this->menu_order->HrefValue = "";
			$this->menu_order->TooltipValue = "";

			// post_type
			$this->post_type->LinkCustomAttributes = "";
			$this->post_type->HrefValue = "";
			$this->post_type->TooltipValue = "";

			// post_mime_type
			$this->post_mime_type->LinkCustomAttributes = "";
			$this->post_mime_type->HrefValue = "";
			$this->post_mime_type->TooltipValue = "";

			// comment_count
			$this->comment_count->LinkCustomAttributes = "";
			$this->comment_count->HrefValue = "";
			$this->comment_count->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$PageCaption = $this->TableCaption();
		$url = ew_CurrentUrl();
		$url = preg_replace('/\?cmd=reset(all){0,1}$/i', '', $url); // Remove cmd=reset / cmd=resetall
		$Breadcrumb->Add("list", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", $url, $this->TableVar);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}

	// Row Custom Action event
	function Row_CustomAction($action, $row) {

		// Return FALSE to abort
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($wp_posts_list)) $wp_posts_list = new cwp_posts_list();

// Page init
$wp_posts_list->Page_Init();

// Page main
$wp_posts_list->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$wp_posts_list->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var wp_posts_list = new ew_Page("wp_posts_list");
wp_posts_list.PageID = "list"; // Page ID
var EW_PAGE_ID = wp_posts_list.PageID; // For backward compatibility

// Form object
var fwp_postslist = new ew_Form("fwp_postslist");
fwp_postslist.FormKeyCountName = '<?php echo $wp_posts_list->FormKeyCountName ?>';

// Form_CustomValidate event
fwp_postslist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fwp_postslist.ValidateRequired = true;
<?php } else { ?>
fwp_postslist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

var fwp_postslistsrch = new ew_Form("fwp_postslistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php $Breadcrumb->Render(); ?>
<?php if ($wp_posts_list->ExportOptions->Visible()) { ?>
<div class="ewListExportOptions"><?php $wp_posts_list->ExportOptions->Render("body") ?></div>
<?php } ?>
<?php
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$wp_posts_list->TotalRecs = $wp_posts->SelectRecordCount();
	} else {
		if ($wp_posts_list->Recordset = $wp_posts_list->LoadRecordset())
			$wp_posts_list->TotalRecs = $wp_posts_list->Recordset->RecordCount();
	}
	$wp_posts_list->StartRec = 1;
	if ($wp_posts_list->DisplayRecs <= 0 || ($wp_posts->Export <> "" && $wp_posts->ExportAll)) // Display all records
		$wp_posts_list->DisplayRecs = $wp_posts_list->TotalRecs;
	if (!($wp_posts->Export <> "" && $wp_posts->ExportAll))
		$wp_posts_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$wp_posts_list->Recordset = $wp_posts_list->LoadRecordset($wp_posts_list->StartRec-1, $wp_posts_list->DisplayRecs);
$wp_posts_list->RenderOtherOptions();
?>
<?php if ($wp_posts->Export == "" && $wp_posts->CurrentAction == "") { ?>
<form name="fwp_postslistsrch" id="fwp_postslistsrch" class="ewForm form-inline" action="<?php echo ew_CurrentPage() ?>">
<table class="ewSearchTable"><tr><td>
<div class="accordion" id="fwp_postslistsrch_SearchGroup">
	<div class="accordion-group">
		<div class="accordion-heading">
<a class="accordion-toggle" data-toggle="collapse" data-parent="#fwp_postslistsrch_SearchGroup" href="#fwp_postslistsrch_SearchBody"><?php echo $Language->Phrase("Search") ?></a>
		</div>
		<div id="fwp_postslistsrch_SearchBody" class="accordion-body collapse in">
			<div class="accordion-inner">
<div id="fwp_postslistsrch_SearchPanel">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="wp_posts">
<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<div class="btn-group ewButtonGroup">
	<div class="input-append">
	<input type="text" name="<?php echo EW_TABLE_BASIC_SEARCH ?>" id="<?php echo EW_TABLE_BASIC_SEARCH ?>" class="input-large" value="<?php echo ew_HtmlEncode($wp_posts_list->BasicSearch->getKeyword()) ?>" placeholder="<?php echo $Language->Phrase("Search") ?>">
	<button class="btn btn-primary ewButton" name="btnsubmit" id="btnsubmit" type="submit"><?php echo $Language->Phrase("QuickSearchBtn") ?></button>
	</div>
	</div>
	<div class="btn-group ewButtonGroup">
	<a class="btn ewShowAll" href="<?php echo $wp_posts_list->PageUrl() ?>cmd=reset"><?php echo $Language->Phrase("ShowAll") ?></a>
</div>
<div id="xsr_2" class="ewRow">
	<label class="inline radio ewRadio" style="white-space: nowrap;"><input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="="<?php if ($wp_posts_list->BasicSearch->getType() == "=") { ?> checked="checked"<?php } ?>><?php echo $Language->Phrase("ExactPhrase") ?></label>
	<label class="inline radio ewRadio" style="white-space: nowrap;"><input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="AND"<?php if ($wp_posts_list->BasicSearch->getType() == "AND") { ?> checked="checked"<?php } ?>><?php echo $Language->Phrase("AllWord") ?></label>
	<label class="inline radio ewRadio" style="white-space: nowrap;"><input type="radio" name="<?php echo EW_TABLE_BASIC_SEARCH_TYPE ?>" value="OR"<?php if ($wp_posts_list->BasicSearch->getType() == "OR") { ?> checked="checked"<?php } ?>><?php echo $Language->Phrase("AnyWord") ?></label>
</div>
</div>
</div>
			</div>
		</div>
	</div>
</div>
</td></tr></table>
</form>
<?php } ?>
<?php $wp_posts_list->ShowPageHeader(); ?>
<?php
$wp_posts_list->ShowMessage();
?>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<form name="fwp_postslist" id="fwp_postslist" class="ewForm form-horizontal" action="<?php echo ew_CurrentPage() ?>" method="post">
<input type="hidden" name="t" value="wp_posts">
<div id="gmp_wp_posts" class="ewGridMiddlePanel">
<?php if ($wp_posts_list->TotalRecs > 0) { ?>
<table id="tbl_wp_postslist" class="ewTable ewTableSeparate">
<?php echo $wp_posts->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$wp_posts_list->RenderListOptions();

// Render list options (header, left)
$wp_posts_list->ListOptions->Render("header", "left");
?>
<?php if ($wp_posts->ID->Visible) { // ID ?>
	<?php if ($wp_posts->SortUrl($wp_posts->ID) == "") { ?>
		<td><div id="elh_wp_posts_ID" class="wp_posts_ID"><div class="ewTableHeaderCaption"><?php echo $wp_posts->ID->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->ID) ?>',1);"><div id="elh_wp_posts_ID" class="wp_posts_ID">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->ID->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->ID->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->ID->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_author->Visible) { // post_author ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_author) == "") { ?>
		<td><div id="elh_wp_posts_post_author" class="wp_posts_post_author"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_author->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_author) ?>',1);"><div id="elh_wp_posts_post_author" class="wp_posts_post_author">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_author->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_author->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_author->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_date->Visible) { // post_date ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_date) == "") { ?>
		<td><div id="elh_wp_posts_post_date" class="wp_posts_post_date"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_date->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_date) ?>',1);"><div id="elh_wp_posts_post_date" class="wp_posts_post_date">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_date->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_date->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_date->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_date_gmt->Visible) { // post_date_gmt ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_date_gmt) == "") { ?>
		<td><div id="elh_wp_posts_post_date_gmt" class="wp_posts_post_date_gmt"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_date_gmt->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_date_gmt) ?>',1);"><div id="elh_wp_posts_post_date_gmt" class="wp_posts_post_date_gmt">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_date_gmt->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_date_gmt->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_date_gmt->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_status->Visible) { // post_status ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_status) == "") { ?>
		<td><div id="elh_wp_posts_post_status" class="wp_posts_post_status"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_status->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_status) ?>',1);"><div id="elh_wp_posts_post_status" class="wp_posts_post_status">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_status->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_status->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_status->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->comment_status->Visible) { // comment_status ?>
	<?php if ($wp_posts->SortUrl($wp_posts->comment_status) == "") { ?>
		<td><div id="elh_wp_posts_comment_status" class="wp_posts_comment_status"><div class="ewTableHeaderCaption"><?php echo $wp_posts->comment_status->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->comment_status) ?>',1);"><div id="elh_wp_posts_comment_status" class="wp_posts_comment_status">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->comment_status->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->comment_status->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->comment_status->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->ping_status->Visible) { // ping_status ?>
	<?php if ($wp_posts->SortUrl($wp_posts->ping_status) == "") { ?>
		<td><div id="elh_wp_posts_ping_status" class="wp_posts_ping_status"><div class="ewTableHeaderCaption"><?php echo $wp_posts->ping_status->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->ping_status) ?>',1);"><div id="elh_wp_posts_ping_status" class="wp_posts_ping_status">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->ping_status->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->ping_status->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->ping_status->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_password->Visible) { // post_password ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_password) == "") { ?>
		<td><div id="elh_wp_posts_post_password" class="wp_posts_post_password"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_password->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_password) ?>',1);"><div id="elh_wp_posts_post_password" class="wp_posts_post_password">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_password->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_password->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_password->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_name->Visible) { // post_name ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_name) == "") { ?>
		<td><div id="elh_wp_posts_post_name" class="wp_posts_post_name"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_name->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_name) ?>',1);"><div id="elh_wp_posts_post_name" class="wp_posts_post_name">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_name->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_name->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_name->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_modified->Visible) { // post_modified ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_modified) == "") { ?>
		<td><div id="elh_wp_posts_post_modified" class="wp_posts_post_modified"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_modified->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_modified) ?>',1);"><div id="elh_wp_posts_post_modified" class="wp_posts_post_modified">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_modified->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_modified->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_modified->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_modified_gmt->Visible) { // post_modified_gmt ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_modified_gmt) == "") { ?>
		<td><div id="elh_wp_posts_post_modified_gmt" class="wp_posts_post_modified_gmt"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_modified_gmt->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_modified_gmt) ?>',1);"><div id="elh_wp_posts_post_modified_gmt" class="wp_posts_post_modified_gmt">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_modified_gmt->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_modified_gmt->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_modified_gmt->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_parent->Visible) { // post_parent ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_parent) == "") { ?>
		<td><div id="elh_wp_posts_post_parent" class="wp_posts_post_parent"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_parent->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_parent) ?>',1);"><div id="elh_wp_posts_post_parent" class="wp_posts_post_parent">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_parent->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_parent->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_parent->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->guid->Visible) { // guid ?>
	<?php if ($wp_posts->SortUrl($wp_posts->guid) == "") { ?>
		<td><div id="elh_wp_posts_guid" class="wp_posts_guid"><div class="ewTableHeaderCaption"><?php echo $wp_posts->guid->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->guid) ?>',1);"><div id="elh_wp_posts_guid" class="wp_posts_guid">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->guid->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->guid->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->guid->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->menu_order->Visible) { // menu_order ?>
	<?php if ($wp_posts->SortUrl($wp_posts->menu_order) == "") { ?>
		<td><div id="elh_wp_posts_menu_order" class="wp_posts_menu_order"><div class="ewTableHeaderCaption"><?php echo $wp_posts->menu_order->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->menu_order) ?>',1);"><div id="elh_wp_posts_menu_order" class="wp_posts_menu_order">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->menu_order->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->menu_order->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->menu_order->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_type->Visible) { // post_type ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_type) == "") { ?>
		<td><div id="elh_wp_posts_post_type" class="wp_posts_post_type"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_type->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_type) ?>',1);"><div id="elh_wp_posts_post_type" class="wp_posts_post_type">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_type->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_type->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_type->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->post_mime_type->Visible) { // post_mime_type ?>
	<?php if ($wp_posts->SortUrl($wp_posts->post_mime_type) == "") { ?>
		<td><div id="elh_wp_posts_post_mime_type" class="wp_posts_post_mime_type"><div class="ewTableHeaderCaption"><?php echo $wp_posts->post_mime_type->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->post_mime_type) ?>',1);"><div id="elh_wp_posts_post_mime_type" class="wp_posts_post_mime_type">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->post_mime_type->FldCaption() ?><?php echo $Language->Phrase("SrchLegend") ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->post_mime_type->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->post_mime_type->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php if ($wp_posts->comment_count->Visible) { // comment_count ?>
	<?php if ($wp_posts->SortUrl($wp_posts->comment_count) == "") { ?>
		<td><div id="elh_wp_posts_comment_count" class="wp_posts_comment_count"><div class="ewTableHeaderCaption"><?php echo $wp_posts->comment_count->FldCaption() ?></div></div></td>
	<?php } else { ?>
		<td><div class="ewPointer" onclick="ew_Sort(event,'<?php echo $wp_posts->SortUrl($wp_posts->comment_count) ?>',1);"><div id="elh_wp_posts_comment_count" class="wp_posts_comment_count">
			<div class="ewTableHeaderBtn"><span class="ewTableHeaderCaption"><?php echo $wp_posts->comment_count->FldCaption() ?></span><span class="ewTableHeaderSort"><?php if ($wp_posts->comment_count->getSort() == "ASC") { ?><span class="caret ewSortUp"></span><?php } elseif ($wp_posts->comment_count->getSort() == "DESC") { ?><span class="caret"></span><?php } ?></span></div>
        </div></div></td>
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$wp_posts_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($wp_posts->ExportAll && $wp_posts->Export <> "") {
	$wp_posts_list->StopRec = $wp_posts_list->TotalRecs;
} else {

	// Set the last record to display
	if ($wp_posts_list->TotalRecs > $wp_posts_list->StartRec + $wp_posts_list->DisplayRecs - 1)
		$wp_posts_list->StopRec = $wp_posts_list->StartRec + $wp_posts_list->DisplayRecs - 1;
	else
		$wp_posts_list->StopRec = $wp_posts_list->TotalRecs;
}
$wp_posts_list->RecCnt = $wp_posts_list->StartRec - 1;
if ($wp_posts_list->Recordset && !$wp_posts_list->Recordset->EOF) {
	$wp_posts_list->Recordset->MoveFirst();
	if (!$bSelectLimit && $wp_posts_list->StartRec > 1)
		$wp_posts_list->Recordset->Move($wp_posts_list->StartRec - 1);
} elseif (!$wp_posts->AllowAddDeleteRow && $wp_posts_list->StopRec == 0) {
	$wp_posts_list->StopRec = $wp_posts->GridAddRowCount;
}

// Initialize aggregate
$wp_posts->RowType = EW_ROWTYPE_AGGREGATEINIT;
$wp_posts->ResetAttrs();
$wp_posts_list->RenderRow();
while ($wp_posts_list->RecCnt < $wp_posts_list->StopRec) {
	$wp_posts_list->RecCnt++;
	if (intval($wp_posts_list->RecCnt) >= intval($wp_posts_list->StartRec)) {
		$wp_posts_list->RowCnt++;

		// Set up key count
		$wp_posts_list->KeyCount = $wp_posts_list->RowIndex;

		// Init row class and style
		$wp_posts->ResetAttrs();
		$wp_posts->CssClass = "";
		if ($wp_posts->CurrentAction == "gridadd") {
		} else {
			$wp_posts_list->LoadRowValues($wp_posts_list->Recordset); // Load row values
		}
		$wp_posts->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$wp_posts->RowAttrs = array_merge($wp_posts->RowAttrs, array('data-rowindex'=>$wp_posts_list->RowCnt, 'id'=>'r' . $wp_posts_list->RowCnt . '_wp_posts', 'data-rowtype'=>$wp_posts->RowType));

		// Render row
		$wp_posts_list->RenderRow();

		// Render list options
		$wp_posts_list->RenderListOptions();
?>
	<tr<?php echo $wp_posts->RowAttributes() ?>>
<?php

// Render list options (body, left)
$wp_posts_list->ListOptions->Render("body", "left", $wp_posts_list->RowCnt);
?>
	<?php if ($wp_posts->ID->Visible) { // ID ?>
		<td<?php echo $wp_posts->ID->CellAttributes() ?>>
<span<?php echo $wp_posts->ID->ViewAttributes() ?>>
<?php echo $wp_posts->ID->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_author->Visible) { // post_author ?>
		<td<?php echo $wp_posts->post_author->CellAttributes() ?>>
<span<?php echo $wp_posts->post_author->ViewAttributes() ?>>
<?php echo $wp_posts->post_author->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_date->Visible) { // post_date ?>
		<td<?php echo $wp_posts->post_date->CellAttributes() ?>>
<span<?php echo $wp_posts->post_date->ViewAttributes() ?>>
<?php echo $wp_posts->post_date->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_date_gmt->Visible) { // post_date_gmt ?>
		<td<?php echo $wp_posts->post_date_gmt->CellAttributes() ?>>
<span<?php echo $wp_posts->post_date_gmt->ViewAttributes() ?>>
<?php echo $wp_posts->post_date_gmt->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_status->Visible) { // post_status ?>
		<td<?php echo $wp_posts->post_status->CellAttributes() ?>>
<span<?php echo $wp_posts->post_status->ViewAttributes() ?>>
<?php echo $wp_posts->post_status->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->comment_status->Visible) { // comment_status ?>
		<td<?php echo $wp_posts->comment_status->CellAttributes() ?>>
<span<?php echo $wp_posts->comment_status->ViewAttributes() ?>>
<?php echo $wp_posts->comment_status->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->ping_status->Visible) { // ping_status ?>
		<td<?php echo $wp_posts->ping_status->CellAttributes() ?>>
<span<?php echo $wp_posts->ping_status->ViewAttributes() ?>>
<?php echo $wp_posts->ping_status->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_password->Visible) { // post_password ?>
		<td<?php echo $wp_posts->post_password->CellAttributes() ?>>
<span<?php echo $wp_posts->post_password->ViewAttributes() ?>>
<?php echo $wp_posts->post_password->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_name->Visible) { // post_name ?>
		<td<?php echo $wp_posts->post_name->CellAttributes() ?>>
<span<?php echo $wp_posts->post_name->ViewAttributes() ?>>
<?php echo $wp_posts->post_name->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_modified->Visible) { // post_modified ?>
		<td<?php echo $wp_posts->post_modified->CellAttributes() ?>>
<span<?php echo $wp_posts->post_modified->ViewAttributes() ?>>
<?php echo $wp_posts->post_modified->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_modified_gmt->Visible) { // post_modified_gmt ?>
		<td<?php echo $wp_posts->post_modified_gmt->CellAttributes() ?>>
<span<?php echo $wp_posts->post_modified_gmt->ViewAttributes() ?>>
<?php echo $wp_posts->post_modified_gmt->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_parent->Visible) { // post_parent ?>
		<td<?php echo $wp_posts->post_parent->CellAttributes() ?>>
<span<?php echo $wp_posts->post_parent->ViewAttributes() ?>>
<?php echo $wp_posts->post_parent->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->guid->Visible) { // guid ?>
		<td<?php echo $wp_posts->guid->CellAttributes() ?>>
<span<?php echo $wp_posts->guid->ViewAttributes() ?>>
<?php echo $wp_posts->guid->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->menu_order->Visible) { // menu_order ?>
		<td<?php echo $wp_posts->menu_order->CellAttributes() ?>>
<span<?php echo $wp_posts->menu_order->ViewAttributes() ?>>
<?php echo $wp_posts->menu_order->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_type->Visible) { // post_type ?>
		<td<?php echo $wp_posts->post_type->CellAttributes() ?>>
<span<?php echo $wp_posts->post_type->ViewAttributes() ?>>
<?php echo $wp_posts->post_type->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->post_mime_type->Visible) { // post_mime_type ?>
		<td<?php echo $wp_posts->post_mime_type->CellAttributes() ?>>
<span<?php echo $wp_posts->post_mime_type->ViewAttributes() ?>>
<?php echo $wp_posts->post_mime_type->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($wp_posts->comment_count->Visible) { // comment_count ?>
		<td<?php echo $wp_posts->comment_count->CellAttributes() ?>>
<span<?php echo $wp_posts->comment_count->ViewAttributes() ?>>
<?php echo $wp_posts->comment_count->ListViewValue() ?></span>
<a id="<?php echo $wp_posts_list->PageObjName . "_row_" . $wp_posts_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$wp_posts_list->ListOptions->Render("body", "right", $wp_posts_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($wp_posts->CurrentAction <> "gridadd")
		$wp_posts_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($wp_posts->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($wp_posts_list->Recordset)
	$wp_posts_list->Recordset->Close();
?>
<div class="ewGridLowerPanel">
<?php if ($wp_posts->CurrentAction <> "gridadd" && $wp_posts->CurrentAction <> "gridedit") { ?>
<form name="ewPagerForm" class="ewForm form-horizontal" action="<?php echo ew_CurrentPage() ?>">
<table class="ewPager">
<tr><td>
<?php if (!isset($wp_posts_list->Pager)) $wp_posts_list->Pager = new cPrevNextPager($wp_posts_list->StartRec, $wp_posts_list->DisplayRecs, $wp_posts_list->TotalRecs) ?>
<?php if ($wp_posts_list->Pager->RecordCount > 0) { ?>
<table cellspacing="0" class="ewStdTable"><tbody><tr><td>
	<?php echo $Language->Phrase("Page") ?>&nbsp;
<div class="input-prepend input-append">
<!--first page button-->
	<?php if ($wp_posts_list->Pager->FirstButton->Enabled) { ?>
	<a class="btn btn-small" href="<?php echo $wp_posts_list->PageUrl() ?>start=<?php echo $wp_posts_list->Pager->FirstButton->Start ?>"><i class="icon-step-backward"></i></a>
	<?php } else { ?>
	<a class="btn btn-small" disabled="disabled"><i class="icon-step-backward"></i></a>
	<?php } ?>
<!--previous page button-->
	<?php if ($wp_posts_list->Pager->PrevButton->Enabled) { ?>
	<a class="btn btn-small" href="<?php echo $wp_posts_list->PageUrl() ?>start=<?php echo $wp_posts_list->Pager->PrevButton->Start ?>"><i class="icon-prev"></i></a>
	<?php } else { ?>
	<a class="btn btn-small" disabled="disabled"><i class="icon-prev"></i></a>
	<?php } ?>
<!--current page number-->
	<input class="input-mini" type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $wp_posts_list->Pager->CurrentPage ?>">
<!--next page button-->
	<?php if ($wp_posts_list->Pager->NextButton->Enabled) { ?>
	<a class="btn btn-small" href="<?php echo $wp_posts_list->PageUrl() ?>start=<?php echo $wp_posts_list->Pager->NextButton->Start ?>"><i class="icon-play"></i></a>
	<?php } else { ?>
	<a class="btn btn-small" disabled="disabled"><i class="icon-play"></i></a>
	<?php } ?>
<!--last page button-->
	<?php if ($wp_posts_list->Pager->LastButton->Enabled) { ?>
	<a class="btn btn-small" href="<?php echo $wp_posts_list->PageUrl() ?>start=<?php echo $wp_posts_list->Pager->LastButton->Start ?>"><i class="icon-step-forward"></i></a>
	<?php } else { ?>
	<a class="btn btn-small" disabled="disabled"><i class="icon-step-forward"></i></a>
	<?php } ?>
</div>
	&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $wp_posts_list->Pager->PageCount ?>
</td>
<td>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $wp_posts_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $wp_posts_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $wp_posts_list->Pager->RecordCount ?>
</td>
</tr></tbody></table>
<?php } else { ?>
	<?php if ($wp_posts_list->SearchWhere == "0=101") { ?>
	<p><?php echo $Language->Phrase("EnterSearchCriteria") ?></p>
	<?php } else { ?>
	<p><?php echo $Language->Phrase("NoRecord") ?></p>
	<?php } ?>
<?php } ?>
</td>
</tr></table>
</form>
<?php } ?>
<div class="ewListOtherOptions">
<?php
	foreach ($wp_posts_list->OtherOptions as &$option)
		$option->Render("body", "bottom");
?>
</div>
</div>
</td></tr></table>
<script type="text/javascript">
fwp_postslistsrch.Init();
fwp_postslist.Init();
<?php if (EW_MOBILE_REFLOW && ew_IsMobile()) { ?>
ew_Reflow();
<?php } ?>
</script>
<?php
$wp_posts_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$wp_posts_list->Page_Terminate();
?>
