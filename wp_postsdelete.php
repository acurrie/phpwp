<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg10.php" ?>
<?php include_once "ewmysql10.php" ?>
<?php include_once "phpfn10.php" ?>
<?php include_once "wp_postsinfo.php" ?>
<?php include_once "userfn10.php" ?>
<?php

//
// Page class
//

$wp_posts_delete = NULL; // Initialize page object first

class cwp_posts_delete extends cwp_posts {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{9787E536-E7C1-412B-A7C2-084F8BF0B798}";

	// Table name
	var $TableName = 'wp_posts';

	// Page object name
	var $PageObjName = 'wp_posts_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-error ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<table class=\"ewStdTable\"><tr><td><div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div></td></tr></table>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (wp_posts)
		if (!isset($GLOBALS["wp_posts"])) {
			$GLOBALS["wp_posts"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["wp_posts"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'wp_posts', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up curent action
		$this->ID->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("wp_postslist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in wp_posts class, wp_postsinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // Delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

// No functions
	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Call Recordset Selecting event
		$this->Recordset_Selecting($this->CurrentFilter);

		// Load List page SQL
		$sSql = $this->SelectSQL();
		if ($offset > -1 && $rowcnt > -1)
			$sSql .= " LIMIT $rowcnt OFFSET $offset";

		// Load recordset
		$rs = ew_LoadRecordset($sSql);

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->ID->setDbValue($rs->fields('ID'));
		$this->post_author->setDbValue($rs->fields('post_author'));
		$this->post_date->setDbValue($rs->fields('post_date'));
		$this->post_date_gmt->setDbValue($rs->fields('post_date_gmt'));
		$this->post_content->setDbValue($rs->fields('post_content'));
		$this->post_title->setDbValue($rs->fields('post_title'));
		$this->post_excerpt->setDbValue($rs->fields('post_excerpt'));
		$this->post_status->setDbValue($rs->fields('post_status'));
		$this->comment_status->setDbValue($rs->fields('comment_status'));
		$this->ping_status->setDbValue($rs->fields('ping_status'));
		$this->post_password->setDbValue($rs->fields('post_password'));
		$this->post_name->setDbValue($rs->fields('post_name'));
		$this->to_ping->setDbValue($rs->fields('to_ping'));
		$this->pinged->setDbValue($rs->fields('pinged'));
		$this->post_modified->setDbValue($rs->fields('post_modified'));
		$this->post_modified_gmt->setDbValue($rs->fields('post_modified_gmt'));
		$this->post_content_filtered->setDbValue($rs->fields('post_content_filtered'));
		$this->post_parent->setDbValue($rs->fields('post_parent'));
		$this->guid->setDbValue($rs->fields('guid'));
		$this->menu_order->setDbValue($rs->fields('menu_order'));
		$this->post_type->setDbValue($rs->fields('post_type'));
		$this->post_mime_type->setDbValue($rs->fields('post_mime_type'));
		$this->comment_count->setDbValue($rs->fields('comment_count'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->ID->DbValue = $row['ID'];
		$this->post_author->DbValue = $row['post_author'];
		$this->post_date->DbValue = $row['post_date'];
		$this->post_date_gmt->DbValue = $row['post_date_gmt'];
		$this->post_content->DbValue = $row['post_content'];
		$this->post_title->DbValue = $row['post_title'];
		$this->post_excerpt->DbValue = $row['post_excerpt'];
		$this->post_status->DbValue = $row['post_status'];
		$this->comment_status->DbValue = $row['comment_status'];
		$this->ping_status->DbValue = $row['ping_status'];
		$this->post_password->DbValue = $row['post_password'];
		$this->post_name->DbValue = $row['post_name'];
		$this->to_ping->DbValue = $row['to_ping'];
		$this->pinged->DbValue = $row['pinged'];
		$this->post_modified->DbValue = $row['post_modified'];
		$this->post_modified_gmt->DbValue = $row['post_modified_gmt'];
		$this->post_content_filtered->DbValue = $row['post_content_filtered'];
		$this->post_parent->DbValue = $row['post_parent'];
		$this->guid->DbValue = $row['guid'];
		$this->menu_order->DbValue = $row['menu_order'];
		$this->post_type->DbValue = $row['post_type'];
		$this->post_mime_type->DbValue = $row['post_mime_type'];
		$this->comment_count->DbValue = $row['comment_count'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// ID
		// post_author
		// post_date
		// post_date_gmt
		// post_content
		// post_title
		// post_excerpt
		// post_status
		// comment_status
		// ping_status
		// post_password
		// post_name
		// to_ping
		// pinged
		// post_modified
		// post_modified_gmt
		// post_content_filtered
		// post_parent
		// guid
		// menu_order
		// post_type
		// post_mime_type
		// comment_count

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// ID
			$this->ID->ViewValue = $this->ID->CurrentValue;
			$this->ID->ViewCustomAttributes = "";

			// post_author
			$this->post_author->ViewValue = $this->post_author->CurrentValue;
			$this->post_author->ViewCustomAttributes = "";

			// post_date
			$this->post_date->ViewValue = $this->post_date->CurrentValue;
			$this->post_date->ViewValue = ew_FormatDateTime($this->post_date->ViewValue, 5);
			$this->post_date->ViewCustomAttributes = "";

			// post_date_gmt
			$this->post_date_gmt->ViewValue = $this->post_date_gmt->CurrentValue;
			$this->post_date_gmt->ViewValue = ew_FormatDateTime($this->post_date_gmt->ViewValue, 5);
			$this->post_date_gmt->ViewCustomAttributes = "";

			// post_status
			$this->post_status->ViewValue = $this->post_status->CurrentValue;
			$this->post_status->ViewCustomAttributes = "";

			// comment_status
			$this->comment_status->ViewValue = $this->comment_status->CurrentValue;
			$this->comment_status->ViewCustomAttributes = "";

			// ping_status
			$this->ping_status->ViewValue = $this->ping_status->CurrentValue;
			$this->ping_status->ViewCustomAttributes = "";

			// post_password
			$this->post_password->ViewValue = $this->post_password->CurrentValue;
			$this->post_password->ViewCustomAttributes = "";

			// post_name
			$this->post_name->ViewValue = $this->post_name->CurrentValue;
			$this->post_name->ViewCustomAttributes = "";

			// post_modified
			$this->post_modified->ViewValue = $this->post_modified->CurrentValue;
			$this->post_modified->ViewValue = ew_FormatDateTime($this->post_modified->ViewValue, 5);
			$this->post_modified->ViewCustomAttributes = "";

			// post_modified_gmt
			$this->post_modified_gmt->ViewValue = $this->post_modified_gmt->CurrentValue;
			$this->post_modified_gmt->ViewValue = ew_FormatDateTime($this->post_modified_gmt->ViewValue, 5);
			$this->post_modified_gmt->ViewCustomAttributes = "";

			// post_parent
			$this->post_parent->ViewValue = $this->post_parent->CurrentValue;
			$this->post_parent->ViewCustomAttributes = "";

			// guid
			$this->guid->ViewValue = $this->guid->CurrentValue;
			$this->guid->ViewCustomAttributes = "";

			// menu_order
			$this->menu_order->ViewValue = $this->menu_order->CurrentValue;
			$this->menu_order->ViewCustomAttributes = "";

			// post_type
			$this->post_type->ViewValue = $this->post_type->CurrentValue;
			$this->post_type->ViewCustomAttributes = "";

			// post_mime_type
			$this->post_mime_type->ViewValue = $this->post_mime_type->CurrentValue;
			$this->post_mime_type->ViewCustomAttributes = "";

			// comment_count
			$this->comment_count->ViewValue = $this->comment_count->CurrentValue;
			$this->comment_count->ViewCustomAttributes = "";

			// ID
			$this->ID->LinkCustomAttributes = "";
			$this->ID->HrefValue = "";
			$this->ID->TooltipValue = "";

			// post_author
			$this->post_author->LinkCustomAttributes = "";
			$this->post_author->HrefValue = "";
			$this->post_author->TooltipValue = "";

			// post_date
			$this->post_date->LinkCustomAttributes = "";
			$this->post_date->HrefValue = "";
			$this->post_date->TooltipValue = "";

			// post_date_gmt
			$this->post_date_gmt->LinkCustomAttributes = "";
			$this->post_date_gmt->HrefValue = "";
			$this->post_date_gmt->TooltipValue = "";

			// post_status
			$this->post_status->LinkCustomAttributes = "";
			$this->post_status->HrefValue = "";
			$this->post_status->TooltipValue = "";

			// comment_status
			$this->comment_status->LinkCustomAttributes = "";
			$this->comment_status->HrefValue = "";
			$this->comment_status->TooltipValue = "";

			// ping_status
			$this->ping_status->LinkCustomAttributes = "";
			$this->ping_status->HrefValue = "";
			$this->ping_status->TooltipValue = "";

			// post_password
			$this->post_password->LinkCustomAttributes = "";
			$this->post_password->HrefValue = "";
			$this->post_password->TooltipValue = "";

			// post_name
			$this->post_name->LinkCustomAttributes = "";
			$this->post_name->HrefValue = "";
			$this->post_name->TooltipValue = "";

			// post_modified
			$this->post_modified->LinkCustomAttributes = "";
			$this->post_modified->HrefValue = "";
			$this->post_modified->TooltipValue = "";

			// post_modified_gmt
			$this->post_modified_gmt->LinkCustomAttributes = "";
			$this->post_modified_gmt->HrefValue = "";
			$this->post_modified_gmt->TooltipValue = "";

			// post_parent
			$this->post_parent->LinkCustomAttributes = "";
			$this->post_parent->HrefValue = "";
			$this->post_parent->TooltipValue = "";

			// guid
			$this->guid->LinkCustomAttributes = "";
			$this->guid->HrefValue = "";
			$this->guid->TooltipValue = "";

			// menu_order
			$this->menu_order->LinkCustomAttributes = "";
			$this->menu_order->HrefValue = "";
			$this->menu_order->TooltipValue = "";

			// post_type
			$this->post_type->LinkCustomAttributes = "";
			$this->post_type->HrefValue = "";
			$this->post_type->TooltipValue = "";

			// post_mime_type
			$this->post_mime_type->LinkCustomAttributes = "";
			$this->post_mime_type->HrefValue = "";
			$this->post_mime_type->TooltipValue = "";

			// comment_count
			$this->comment_count->LinkCustomAttributes = "";
			$this->comment_count->HrefValue = "";
			$this->comment_count->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $conn, $Language, $Security;
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;

		//} else {
		//	$this->LoadRowValues($rs); // Load row values

		}
		$conn->BeginTrans();

		// Clone old rows
		$rsold = ($rs) ? $rs->GetRows() : array();
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['ID'];
				$conn->raiseErrorFn = 'ew_ErrorFn';
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
		} else {
			$conn->RollbackTrans(); // Rollback changes
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$PageCaption = $this->TableCaption();
		$Breadcrumb->Add("list", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", "wp_postslist.php", $this->TableVar);
		$PageCaption = $Language->Phrase("delete");
		$Breadcrumb->Add("delete", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", ew_CurrentUrl(), $this->TableVar);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($wp_posts_delete)) $wp_posts_delete = new cwp_posts_delete();

// Page init
$wp_posts_delete->Page_Init();

// Page main
$wp_posts_delete->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$wp_posts_delete->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var wp_posts_delete = new ew_Page("wp_posts_delete");
wp_posts_delete.PageID = "delete"; // Page ID
var EW_PAGE_ID = wp_posts_delete.PageID; // For backward compatibility

// Form object
var fwp_postsdelete = new ew_Form("fwp_postsdelete");

// Form_CustomValidate event
fwp_postsdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fwp_postsdelete.ValidateRequired = true;
<?php } else { ?>
fwp_postsdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($wp_posts_delete->Recordset = $wp_posts_delete->LoadRecordset())
	$wp_posts_deleteTotalRecs = $wp_posts_delete->Recordset->RecordCount(); // Get record count
if ($wp_posts_deleteTotalRecs <= 0) { // No record found, exit
	if ($wp_posts_delete->Recordset)
		$wp_posts_delete->Recordset->Close();
	$wp_posts_delete->Page_Terminate("wp_postslist.php"); // Return to list
}
?>
<?php $Breadcrumb->Render(); ?>
<?php $wp_posts_delete->ShowPageHeader(); ?>
<?php
$wp_posts_delete->ShowMessage();
?>
<form name="fwp_postsdelete" id="fwp_postsdelete" class="ewForm form-horizontal" action="<?php echo ew_CurrentPage() ?>" method="post">
<input type="hidden" name="t" value="wp_posts">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($wp_posts_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div class="ewGridMiddlePanel">
<table id="tbl_wp_postsdelete" class="ewTable ewTableSeparate">
<?php echo $wp_posts->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
<?php if ($wp_posts->ID->Visible) { // ID ?>
		<td><span id="elh_wp_posts_ID" class="wp_posts_ID"><?php echo $wp_posts->ID->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_author->Visible) { // post_author ?>
		<td><span id="elh_wp_posts_post_author" class="wp_posts_post_author"><?php echo $wp_posts->post_author->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_date->Visible) { // post_date ?>
		<td><span id="elh_wp_posts_post_date" class="wp_posts_post_date"><?php echo $wp_posts->post_date->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_date_gmt->Visible) { // post_date_gmt ?>
		<td><span id="elh_wp_posts_post_date_gmt" class="wp_posts_post_date_gmt"><?php echo $wp_posts->post_date_gmt->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_status->Visible) { // post_status ?>
		<td><span id="elh_wp_posts_post_status" class="wp_posts_post_status"><?php echo $wp_posts->post_status->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->comment_status->Visible) { // comment_status ?>
		<td><span id="elh_wp_posts_comment_status" class="wp_posts_comment_status"><?php echo $wp_posts->comment_status->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->ping_status->Visible) { // ping_status ?>
		<td><span id="elh_wp_posts_ping_status" class="wp_posts_ping_status"><?php echo $wp_posts->ping_status->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_password->Visible) { // post_password ?>
		<td><span id="elh_wp_posts_post_password" class="wp_posts_post_password"><?php echo $wp_posts->post_password->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_name->Visible) { // post_name ?>
		<td><span id="elh_wp_posts_post_name" class="wp_posts_post_name"><?php echo $wp_posts->post_name->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_modified->Visible) { // post_modified ?>
		<td><span id="elh_wp_posts_post_modified" class="wp_posts_post_modified"><?php echo $wp_posts->post_modified->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_modified_gmt->Visible) { // post_modified_gmt ?>
		<td><span id="elh_wp_posts_post_modified_gmt" class="wp_posts_post_modified_gmt"><?php echo $wp_posts->post_modified_gmt->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_parent->Visible) { // post_parent ?>
		<td><span id="elh_wp_posts_post_parent" class="wp_posts_post_parent"><?php echo $wp_posts->post_parent->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->guid->Visible) { // guid ?>
		<td><span id="elh_wp_posts_guid" class="wp_posts_guid"><?php echo $wp_posts->guid->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->menu_order->Visible) { // menu_order ?>
		<td><span id="elh_wp_posts_menu_order" class="wp_posts_menu_order"><?php echo $wp_posts->menu_order->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_type->Visible) { // post_type ?>
		<td><span id="elh_wp_posts_post_type" class="wp_posts_post_type"><?php echo $wp_posts->post_type->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->post_mime_type->Visible) { // post_mime_type ?>
		<td><span id="elh_wp_posts_post_mime_type" class="wp_posts_post_mime_type"><?php echo $wp_posts->post_mime_type->FldCaption() ?></span></td>
<?php } ?>
<?php if ($wp_posts->comment_count->Visible) { // comment_count ?>
		<td><span id="elh_wp_posts_comment_count" class="wp_posts_comment_count"><?php echo $wp_posts->comment_count->FldCaption() ?></span></td>
<?php } ?>
	</tr>
	</thead>
	<tbody>
<?php
$wp_posts_delete->RecCnt = 0;
$i = 0;
while (!$wp_posts_delete->Recordset->EOF) {
	$wp_posts_delete->RecCnt++;
	$wp_posts_delete->RowCnt++;

	// Set row properties
	$wp_posts->ResetAttrs();
	$wp_posts->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$wp_posts_delete->LoadRowValues($wp_posts_delete->Recordset);

	// Render row
	$wp_posts_delete->RenderRow();
?>
	<tr<?php echo $wp_posts->RowAttributes() ?>>
<?php if ($wp_posts->ID->Visible) { // ID ?>
		<td<?php echo $wp_posts->ID->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_ID" class="control-group wp_posts_ID">
<span<?php echo $wp_posts->ID->ViewAttributes() ?>>
<?php echo $wp_posts->ID->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_author->Visible) { // post_author ?>
		<td<?php echo $wp_posts->post_author->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_author" class="control-group wp_posts_post_author">
<span<?php echo $wp_posts->post_author->ViewAttributes() ?>>
<?php echo $wp_posts->post_author->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_date->Visible) { // post_date ?>
		<td<?php echo $wp_posts->post_date->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_date" class="control-group wp_posts_post_date">
<span<?php echo $wp_posts->post_date->ViewAttributes() ?>>
<?php echo $wp_posts->post_date->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_date_gmt->Visible) { // post_date_gmt ?>
		<td<?php echo $wp_posts->post_date_gmt->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_date_gmt" class="control-group wp_posts_post_date_gmt">
<span<?php echo $wp_posts->post_date_gmt->ViewAttributes() ?>>
<?php echo $wp_posts->post_date_gmt->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_status->Visible) { // post_status ?>
		<td<?php echo $wp_posts->post_status->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_status" class="control-group wp_posts_post_status">
<span<?php echo $wp_posts->post_status->ViewAttributes() ?>>
<?php echo $wp_posts->post_status->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->comment_status->Visible) { // comment_status ?>
		<td<?php echo $wp_posts->comment_status->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_comment_status" class="control-group wp_posts_comment_status">
<span<?php echo $wp_posts->comment_status->ViewAttributes() ?>>
<?php echo $wp_posts->comment_status->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->ping_status->Visible) { // ping_status ?>
		<td<?php echo $wp_posts->ping_status->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_ping_status" class="control-group wp_posts_ping_status">
<span<?php echo $wp_posts->ping_status->ViewAttributes() ?>>
<?php echo $wp_posts->ping_status->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_password->Visible) { // post_password ?>
		<td<?php echo $wp_posts->post_password->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_password" class="control-group wp_posts_post_password">
<span<?php echo $wp_posts->post_password->ViewAttributes() ?>>
<?php echo $wp_posts->post_password->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_name->Visible) { // post_name ?>
		<td<?php echo $wp_posts->post_name->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_name" class="control-group wp_posts_post_name">
<span<?php echo $wp_posts->post_name->ViewAttributes() ?>>
<?php echo $wp_posts->post_name->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_modified->Visible) { // post_modified ?>
		<td<?php echo $wp_posts->post_modified->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_modified" class="control-group wp_posts_post_modified">
<span<?php echo $wp_posts->post_modified->ViewAttributes() ?>>
<?php echo $wp_posts->post_modified->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_modified_gmt->Visible) { // post_modified_gmt ?>
		<td<?php echo $wp_posts->post_modified_gmt->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_modified_gmt" class="control-group wp_posts_post_modified_gmt">
<span<?php echo $wp_posts->post_modified_gmt->ViewAttributes() ?>>
<?php echo $wp_posts->post_modified_gmt->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_parent->Visible) { // post_parent ?>
		<td<?php echo $wp_posts->post_parent->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_parent" class="control-group wp_posts_post_parent">
<span<?php echo $wp_posts->post_parent->ViewAttributes() ?>>
<?php echo $wp_posts->post_parent->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->guid->Visible) { // guid ?>
		<td<?php echo $wp_posts->guid->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_guid" class="control-group wp_posts_guid">
<span<?php echo $wp_posts->guid->ViewAttributes() ?>>
<?php echo $wp_posts->guid->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->menu_order->Visible) { // menu_order ?>
		<td<?php echo $wp_posts->menu_order->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_menu_order" class="control-group wp_posts_menu_order">
<span<?php echo $wp_posts->menu_order->ViewAttributes() ?>>
<?php echo $wp_posts->menu_order->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_type->Visible) { // post_type ?>
		<td<?php echo $wp_posts->post_type->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_type" class="control-group wp_posts_post_type">
<span<?php echo $wp_posts->post_type->ViewAttributes() ?>>
<?php echo $wp_posts->post_type->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->post_mime_type->Visible) { // post_mime_type ?>
		<td<?php echo $wp_posts->post_mime_type->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_post_mime_type" class="control-group wp_posts_post_mime_type">
<span<?php echo $wp_posts->post_mime_type->ViewAttributes() ?>>
<?php echo $wp_posts->post_mime_type->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
<?php if ($wp_posts->comment_count->Visible) { // comment_count ?>
		<td<?php echo $wp_posts->comment_count->CellAttributes() ?>>
<span id="el<?php echo $wp_posts_delete->RowCnt ?>_wp_posts_comment_count" class="control-group wp_posts_comment_count">
<span<?php echo $wp_posts->comment_count->ViewAttributes() ?>>
<?php echo $wp_posts->comment_count->ListViewValue() ?></span>
</span>
</td>
<?php } ?>
	</tr>
<?php
	$wp_posts_delete->Recordset->MoveNext();
}
$wp_posts_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</td></tr></table>
<div class="btn-group ewButtonGroup">
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("DeleteBtn") ?></button>
</div>
</form>
<script type="text/javascript">
fwp_postsdelete.Init();
</script>
<?php
$wp_posts_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$wp_posts_delete->Page_Terminate();
?>
