<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg10.php" ?>
<?php include_once "ewmysql10.php" ?>
<?php include_once "phpfn10.php" ?>
<?php include_once "wp_postsinfo.php" ?>
<?php include_once "userfn10.php" ?>
<?php

//
// Page class
//

$wp_posts_view = NULL; // Initialize page object first

class cwp_posts_view extends cwp_posts {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{9787E536-E7C1-412B-A7C2-084F8BF0B798}";

	// Table name
	var $TableName = 'wp_posts';

	// Page object name
	var $PageObjName = 'wp_posts_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-error ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<table class=\"ewStdTable\"><tr><td><div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div></td></tr></table>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (wp_posts)
		if (!isset($GLOBALS["wp_posts"])) {
			$GLOBALS["wp_posts"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["wp_posts"];
		}
		$KeyUrl = "";
		if (@$_GET["ID"] <> "") {
			$this->RecKey["ID"] = $_GET["ID"];
			$KeyUrl .= "&ID=" . urlencode($this->RecKey["ID"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'wp_posts', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "span";
		$this->ExportOptions->TagClassName = "ewExportOption";

		// Other options
		$this->OtherOptions['action'] = new cListOptions();
		$this->OtherOptions['action']->Tag = "span";
		$this->OtherOptions['action']->TagClassName = "ewActionOption";
		$this->OtherOptions['detail'] = new cListOptions();
		$this->OtherOptions['detail']->Tag = "span";
		$this->OtherOptions['detail']->TagClassName = "ewDetailOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up curent action
		$this->ID->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $OtherOptions = array(); // Other options
	var $DisplayRecs = 1;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;

		// Set up Breadcrumb
		$this->SetupBreadcrumb();
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["ID"] <> "") {
				$this->ID->setQueryStringValue($_GET["ID"]);
				$this->RecKey["ID"] = $this->ID->QueryStringValue;
			} else {
				$sReturnUrl = "wp_postslist.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "wp_postslist.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "wp_postslist.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up other options
	function SetupOtherOptions() {
		global $Language, $Security;
		$options = &$this->OtherOptions;
		$option = &$options["action"];

		// Add
		$item = &$option->Add("add");
		$item->Body = "<a class=\"ewAction ewAdd\" href=\"" . ew_HtmlEncode($this->AddUrl) . "\">" . $Language->Phrase("ViewPageAddLink") . "</a>";
		$item->Visible = ($this->AddUrl <> "");

		// Edit
		$item = &$option->Add("edit");
		$item->Body = "<a class=\"ewAction ewEdit\" href=\"" . ew_HtmlEncode($this->EditUrl) . "\">" . $Language->Phrase("ViewPageEditLink") . "</a>";
		$item->Visible = ($this->EditUrl <> "");

		// Copy
		$item = &$option->Add("copy");
		$item->Body = "<a class=\"ewAction ewCopy\" href=\"" . ew_HtmlEncode($this->CopyUrl) . "\">" . $Language->Phrase("ViewPageCopyLink") . "</a>";
		$item->Visible = ($this->CopyUrl <> "");

		// Delete
		$item = &$option->Add("delete");
		$item->Body = "<a class=\"ewAction ewDelete\" href=\"" . ew_HtmlEncode($this->DeleteUrl) . "\">" . $Language->Phrase("ViewPageDeleteLink") . "</a>";
		$item->Visible = ($this->DeleteUrl <> "");

		// Set up options default
		foreach ($options as &$option) {
			$option->UseDropDownButton = FALSE;
			$option->UseButtonGroup = TRUE;
			$item = &$option->Add($option->GroupOptionName);
			$item->Body = "";
			$item->Visible = FALSE;
		}
		$options["detail"]->DropDownButtonPhrase = $Language->Phrase("ButtonDetails");
		$options["action"]->DropDownButtonPhrase = $Language->Phrase("ButtonActions");
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->ID->setDbValue($rs->fields('ID'));
		$this->post_author->setDbValue($rs->fields('post_author'));
		$this->post_date->setDbValue($rs->fields('post_date'));
		$this->post_date_gmt->setDbValue($rs->fields('post_date_gmt'));
		$this->post_content->setDbValue($rs->fields('post_content'));
		$this->post_title->setDbValue($rs->fields('post_title'));
		$this->post_excerpt->setDbValue($rs->fields('post_excerpt'));
		$this->post_status->setDbValue($rs->fields('post_status'));
		$this->comment_status->setDbValue($rs->fields('comment_status'));
		$this->ping_status->setDbValue($rs->fields('ping_status'));
		$this->post_password->setDbValue($rs->fields('post_password'));
		$this->post_name->setDbValue($rs->fields('post_name'));
		$this->to_ping->setDbValue($rs->fields('to_ping'));
		$this->pinged->setDbValue($rs->fields('pinged'));
		$this->post_modified->setDbValue($rs->fields('post_modified'));
		$this->post_modified_gmt->setDbValue($rs->fields('post_modified_gmt'));
		$this->post_content_filtered->setDbValue($rs->fields('post_content_filtered'));
		$this->post_parent->setDbValue($rs->fields('post_parent'));
		$this->guid->setDbValue($rs->fields('guid'));
		$this->menu_order->setDbValue($rs->fields('menu_order'));
		$this->post_type->setDbValue($rs->fields('post_type'));
		$this->post_mime_type->setDbValue($rs->fields('post_mime_type'));
		$this->comment_count->setDbValue($rs->fields('comment_count'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->ID->DbValue = $row['ID'];
		$this->post_author->DbValue = $row['post_author'];
		$this->post_date->DbValue = $row['post_date'];
		$this->post_date_gmt->DbValue = $row['post_date_gmt'];
		$this->post_content->DbValue = $row['post_content'];
		$this->post_title->DbValue = $row['post_title'];
		$this->post_excerpt->DbValue = $row['post_excerpt'];
		$this->post_status->DbValue = $row['post_status'];
		$this->comment_status->DbValue = $row['comment_status'];
		$this->ping_status->DbValue = $row['ping_status'];
		$this->post_password->DbValue = $row['post_password'];
		$this->post_name->DbValue = $row['post_name'];
		$this->to_ping->DbValue = $row['to_ping'];
		$this->pinged->DbValue = $row['pinged'];
		$this->post_modified->DbValue = $row['post_modified'];
		$this->post_modified_gmt->DbValue = $row['post_modified_gmt'];
		$this->post_content_filtered->DbValue = $row['post_content_filtered'];
		$this->post_parent->DbValue = $row['post_parent'];
		$this->guid->DbValue = $row['guid'];
		$this->menu_order->DbValue = $row['menu_order'];
		$this->post_type->DbValue = $row['post_type'];
		$this->post_mime_type->DbValue = $row['post_mime_type'];
		$this->comment_count->DbValue = $row['comment_count'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();
		$this->SetupOtherOptions();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// ID
		// post_author
		// post_date
		// post_date_gmt
		// post_content
		// post_title
		// post_excerpt
		// post_status
		// comment_status
		// ping_status
		// post_password
		// post_name
		// to_ping
		// pinged
		// post_modified
		// post_modified_gmt
		// post_content_filtered
		// post_parent
		// guid
		// menu_order
		// post_type
		// post_mime_type
		// comment_count

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// ID
			$this->ID->ViewValue = $this->ID->CurrentValue;
			$this->ID->ViewCustomAttributes = "";

			// post_author
			$this->post_author->ViewValue = $this->post_author->CurrentValue;
			$this->post_author->ViewCustomAttributes = "";

			// post_date
			$this->post_date->ViewValue = $this->post_date->CurrentValue;
			$this->post_date->ViewValue = ew_FormatDateTime($this->post_date->ViewValue, 5);
			$this->post_date->ViewCustomAttributes = "";

			// post_date_gmt
			$this->post_date_gmt->ViewValue = $this->post_date_gmt->CurrentValue;
			$this->post_date_gmt->ViewValue = ew_FormatDateTime($this->post_date_gmt->ViewValue, 5);
			$this->post_date_gmt->ViewCustomAttributes = "";

			// post_content
			$this->post_content->ViewValue = $this->post_content->CurrentValue;
			$this->post_content->ViewCustomAttributes = "";

			// post_title
			$this->post_title->ViewValue = $this->post_title->CurrentValue;
			$this->post_title->ViewCustomAttributes = "";

			// post_excerpt
			$this->post_excerpt->ViewValue = $this->post_excerpt->CurrentValue;
			$this->post_excerpt->ViewCustomAttributes = "";

			// post_status
			$this->post_status->ViewValue = $this->post_status->CurrentValue;
			$this->post_status->ViewCustomAttributes = "";

			// comment_status
			$this->comment_status->ViewValue = $this->comment_status->CurrentValue;
			$this->comment_status->ViewCustomAttributes = "";

			// ping_status
			$this->ping_status->ViewValue = $this->ping_status->CurrentValue;
			$this->ping_status->ViewCustomAttributes = "";

			// post_password
			$this->post_password->ViewValue = $this->post_password->CurrentValue;
			$this->post_password->ViewCustomAttributes = "";

			// post_name
			$this->post_name->ViewValue = $this->post_name->CurrentValue;
			$this->post_name->ViewCustomAttributes = "";

			// to_ping
			$this->to_ping->ViewValue = $this->to_ping->CurrentValue;
			$this->to_ping->ViewCustomAttributes = "";

			// pinged
			$this->pinged->ViewValue = $this->pinged->CurrentValue;
			$this->pinged->ViewCustomAttributes = "";

			// post_modified
			$this->post_modified->ViewValue = $this->post_modified->CurrentValue;
			$this->post_modified->ViewValue = ew_FormatDateTime($this->post_modified->ViewValue, 5);
			$this->post_modified->ViewCustomAttributes = "";

			// post_modified_gmt
			$this->post_modified_gmt->ViewValue = $this->post_modified_gmt->CurrentValue;
			$this->post_modified_gmt->ViewValue = ew_FormatDateTime($this->post_modified_gmt->ViewValue, 5);
			$this->post_modified_gmt->ViewCustomAttributes = "";

			// post_content_filtered
			$this->post_content_filtered->ViewValue = $this->post_content_filtered->CurrentValue;
			$this->post_content_filtered->ViewCustomAttributes = "";

			// post_parent
			$this->post_parent->ViewValue = $this->post_parent->CurrentValue;
			$this->post_parent->ViewCustomAttributes = "";

			// guid
			$this->guid->ViewValue = $this->guid->CurrentValue;
			$this->guid->ViewCustomAttributes = "";

			// menu_order
			$this->menu_order->ViewValue = $this->menu_order->CurrentValue;
			$this->menu_order->ViewCustomAttributes = "";

			// post_type
			$this->post_type->ViewValue = $this->post_type->CurrentValue;
			$this->post_type->ViewCustomAttributes = "";

			// post_mime_type
			$this->post_mime_type->ViewValue = $this->post_mime_type->CurrentValue;
			$this->post_mime_type->ViewCustomAttributes = "";

			// comment_count
			$this->comment_count->ViewValue = $this->comment_count->CurrentValue;
			$this->comment_count->ViewCustomAttributes = "";

			// ID
			$this->ID->LinkCustomAttributes = "";
			$this->ID->HrefValue = "";
			$this->ID->TooltipValue = "";

			// post_author
			$this->post_author->LinkCustomAttributes = "";
			$this->post_author->HrefValue = "";
			$this->post_author->TooltipValue = "";

			// post_date
			$this->post_date->LinkCustomAttributes = "";
			$this->post_date->HrefValue = "";
			$this->post_date->TooltipValue = "";

			// post_date_gmt
			$this->post_date_gmt->LinkCustomAttributes = "";
			$this->post_date_gmt->HrefValue = "";
			$this->post_date_gmt->TooltipValue = "";

			// post_content
			$this->post_content->LinkCustomAttributes = "";
			$this->post_content->HrefValue = "";
			$this->post_content->TooltipValue = "";

			// post_title
			$this->post_title->LinkCustomAttributes = "";
			$this->post_title->HrefValue = "";
			$this->post_title->TooltipValue = "";

			// post_excerpt
			$this->post_excerpt->LinkCustomAttributes = "";
			$this->post_excerpt->HrefValue = "";
			$this->post_excerpt->TooltipValue = "";

			// post_status
			$this->post_status->LinkCustomAttributes = "";
			$this->post_status->HrefValue = "";
			$this->post_status->TooltipValue = "";

			// comment_status
			$this->comment_status->LinkCustomAttributes = "";
			$this->comment_status->HrefValue = "";
			$this->comment_status->TooltipValue = "";

			// ping_status
			$this->ping_status->LinkCustomAttributes = "";
			$this->ping_status->HrefValue = "";
			$this->ping_status->TooltipValue = "";

			// post_password
			$this->post_password->LinkCustomAttributes = "";
			$this->post_password->HrefValue = "";
			$this->post_password->TooltipValue = "";

			// post_name
			$this->post_name->LinkCustomAttributes = "";
			$this->post_name->HrefValue = "";
			$this->post_name->TooltipValue = "";

			// to_ping
			$this->to_ping->LinkCustomAttributes = "";
			$this->to_ping->HrefValue = "";
			$this->to_ping->TooltipValue = "";

			// pinged
			$this->pinged->LinkCustomAttributes = "";
			$this->pinged->HrefValue = "";
			$this->pinged->TooltipValue = "";

			// post_modified
			$this->post_modified->LinkCustomAttributes = "";
			$this->post_modified->HrefValue = "";
			$this->post_modified->TooltipValue = "";

			// post_modified_gmt
			$this->post_modified_gmt->LinkCustomAttributes = "";
			$this->post_modified_gmt->HrefValue = "";
			$this->post_modified_gmt->TooltipValue = "";

			// post_content_filtered
			$this->post_content_filtered->LinkCustomAttributes = "";
			$this->post_content_filtered->HrefValue = "";
			$this->post_content_filtered->TooltipValue = "";

			// post_parent
			$this->post_parent->LinkCustomAttributes = "";
			$this->post_parent->HrefValue = "";
			$this->post_parent->TooltipValue = "";

			// guid
			$this->guid->LinkCustomAttributes = "";
			$this->guid->HrefValue = "";
			$this->guid->TooltipValue = "";

			// menu_order
			$this->menu_order->LinkCustomAttributes = "";
			$this->menu_order->HrefValue = "";
			$this->menu_order->TooltipValue = "";

			// post_type
			$this->post_type->LinkCustomAttributes = "";
			$this->post_type->HrefValue = "";
			$this->post_type->TooltipValue = "";

			// post_mime_type
			$this->post_mime_type->LinkCustomAttributes = "";
			$this->post_mime_type->HrefValue = "";
			$this->post_mime_type->TooltipValue = "";

			// comment_count
			$this->comment_count->LinkCustomAttributes = "";
			$this->comment_count->HrefValue = "";
			$this->comment_count->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$PageCaption = $this->TableCaption();
		$Breadcrumb->Add("list", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", "wp_postslist.php", $this->TableVar);
		$PageCaption = $Language->Phrase("view");
		$Breadcrumb->Add("view", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", ew_CurrentUrl(), $this->TableVar);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($wp_posts_view)) $wp_posts_view = new cwp_posts_view();

// Page init
$wp_posts_view->Page_Init();

// Page main
$wp_posts_view->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$wp_posts_view->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var wp_posts_view = new ew_Page("wp_posts_view");
wp_posts_view.PageID = "view"; // Page ID
var EW_PAGE_ID = wp_posts_view.PageID; // For backward compatibility

// Form object
var fwp_postsview = new ew_Form("fwp_postsview");

// Form_CustomValidate event
fwp_postsview.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fwp_postsview.ValidateRequired = true;
<?php } else { ?>
fwp_postsview.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php $Breadcrumb->Render(); ?>
<div class="ewViewExportOptions">
<?php $wp_posts_view->ExportOptions->Render("body") ?>
<?php if (!$wp_posts_view->ExportOptions->UseDropDownButton) { ?>
</div>
<div class="ewViewOtherOptions">
<?php } ?>
<?php
	foreach ($wp_posts_view->OtherOptions as &$option)
		$option->Render("body");
?>
</div>
<?php $wp_posts_view->ShowPageHeader(); ?>
<?php
$wp_posts_view->ShowMessage();
?>
<form name="fwp_postsview" id="fwp_postsview" class="ewForm form-horizontal" action="<?php echo ew_CurrentPage() ?>" method="post">
<input type="hidden" name="t" value="wp_posts">
<table cellspacing="0" class="ewGrid"><tr><td>
<table id="tbl_wp_postsview" class="table table-bordered table-striped">
<?php if ($wp_posts->ID->Visible) { // ID ?>
	<tr id="r_ID">
		<td><span id="elh_wp_posts_ID"><?php echo $wp_posts->ID->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->ID->CellAttributes() ?>>
<span id="el_wp_posts_ID" class="control-group">
<span<?php echo $wp_posts->ID->ViewAttributes() ?>>
<?php echo $wp_posts->ID->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_author->Visible) { // post_author ?>
	<tr id="r_post_author">
		<td><span id="elh_wp_posts_post_author"><?php echo $wp_posts->post_author->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_author->CellAttributes() ?>>
<span id="el_wp_posts_post_author" class="control-group">
<span<?php echo $wp_posts->post_author->ViewAttributes() ?>>
<?php echo $wp_posts->post_author->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_date->Visible) { // post_date ?>
	<tr id="r_post_date">
		<td><span id="elh_wp_posts_post_date"><?php echo $wp_posts->post_date->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_date->CellAttributes() ?>>
<span id="el_wp_posts_post_date" class="control-group">
<span<?php echo $wp_posts->post_date->ViewAttributes() ?>>
<?php echo $wp_posts->post_date->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_date_gmt->Visible) { // post_date_gmt ?>
	<tr id="r_post_date_gmt">
		<td><span id="elh_wp_posts_post_date_gmt"><?php echo $wp_posts->post_date_gmt->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_date_gmt->CellAttributes() ?>>
<span id="el_wp_posts_post_date_gmt" class="control-group">
<span<?php echo $wp_posts->post_date_gmt->ViewAttributes() ?>>
<?php echo $wp_posts->post_date_gmt->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_content->Visible) { // post_content ?>
	<tr id="r_post_content">
		<td><span id="elh_wp_posts_post_content"><?php echo $wp_posts->post_content->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_content->CellAttributes() ?>>
<span id="el_wp_posts_post_content" class="control-group">
<span<?php echo $wp_posts->post_content->ViewAttributes() ?>>
<?php echo $wp_posts->post_content->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_title->Visible) { // post_title ?>
	<tr id="r_post_title">
		<td><span id="elh_wp_posts_post_title"><?php echo $wp_posts->post_title->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_title->CellAttributes() ?>>
<span id="el_wp_posts_post_title" class="control-group">
<span<?php echo $wp_posts->post_title->ViewAttributes() ?>>
<?php echo $wp_posts->post_title->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_excerpt->Visible) { // post_excerpt ?>
	<tr id="r_post_excerpt">
		<td><span id="elh_wp_posts_post_excerpt"><?php echo $wp_posts->post_excerpt->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_excerpt->CellAttributes() ?>>
<span id="el_wp_posts_post_excerpt" class="control-group">
<span<?php echo $wp_posts->post_excerpt->ViewAttributes() ?>>
<?php echo $wp_posts->post_excerpt->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_status->Visible) { // post_status ?>
	<tr id="r_post_status">
		<td><span id="elh_wp_posts_post_status"><?php echo $wp_posts->post_status->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_status->CellAttributes() ?>>
<span id="el_wp_posts_post_status" class="control-group">
<span<?php echo $wp_posts->post_status->ViewAttributes() ?>>
<?php echo $wp_posts->post_status->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->comment_status->Visible) { // comment_status ?>
	<tr id="r_comment_status">
		<td><span id="elh_wp_posts_comment_status"><?php echo $wp_posts->comment_status->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->comment_status->CellAttributes() ?>>
<span id="el_wp_posts_comment_status" class="control-group">
<span<?php echo $wp_posts->comment_status->ViewAttributes() ?>>
<?php echo $wp_posts->comment_status->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->ping_status->Visible) { // ping_status ?>
	<tr id="r_ping_status">
		<td><span id="elh_wp_posts_ping_status"><?php echo $wp_posts->ping_status->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->ping_status->CellAttributes() ?>>
<span id="el_wp_posts_ping_status" class="control-group">
<span<?php echo $wp_posts->ping_status->ViewAttributes() ?>>
<?php echo $wp_posts->ping_status->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_password->Visible) { // post_password ?>
	<tr id="r_post_password">
		<td><span id="elh_wp_posts_post_password"><?php echo $wp_posts->post_password->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_password->CellAttributes() ?>>
<span id="el_wp_posts_post_password" class="control-group">
<span<?php echo $wp_posts->post_password->ViewAttributes() ?>>
<?php echo $wp_posts->post_password->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_name->Visible) { // post_name ?>
	<tr id="r_post_name">
		<td><span id="elh_wp_posts_post_name"><?php echo $wp_posts->post_name->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_name->CellAttributes() ?>>
<span id="el_wp_posts_post_name" class="control-group">
<span<?php echo $wp_posts->post_name->ViewAttributes() ?>>
<?php echo $wp_posts->post_name->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->to_ping->Visible) { // to_ping ?>
	<tr id="r_to_ping">
		<td><span id="elh_wp_posts_to_ping"><?php echo $wp_posts->to_ping->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->to_ping->CellAttributes() ?>>
<span id="el_wp_posts_to_ping" class="control-group">
<span<?php echo $wp_posts->to_ping->ViewAttributes() ?>>
<?php echo $wp_posts->to_ping->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->pinged->Visible) { // pinged ?>
	<tr id="r_pinged">
		<td><span id="elh_wp_posts_pinged"><?php echo $wp_posts->pinged->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->pinged->CellAttributes() ?>>
<span id="el_wp_posts_pinged" class="control-group">
<span<?php echo $wp_posts->pinged->ViewAttributes() ?>>
<?php echo $wp_posts->pinged->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_modified->Visible) { // post_modified ?>
	<tr id="r_post_modified">
		<td><span id="elh_wp_posts_post_modified"><?php echo $wp_posts->post_modified->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_modified->CellAttributes() ?>>
<span id="el_wp_posts_post_modified" class="control-group">
<span<?php echo $wp_posts->post_modified->ViewAttributes() ?>>
<?php echo $wp_posts->post_modified->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_modified_gmt->Visible) { // post_modified_gmt ?>
	<tr id="r_post_modified_gmt">
		<td><span id="elh_wp_posts_post_modified_gmt"><?php echo $wp_posts->post_modified_gmt->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_modified_gmt->CellAttributes() ?>>
<span id="el_wp_posts_post_modified_gmt" class="control-group">
<span<?php echo $wp_posts->post_modified_gmt->ViewAttributes() ?>>
<?php echo $wp_posts->post_modified_gmt->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_content_filtered->Visible) { // post_content_filtered ?>
	<tr id="r_post_content_filtered">
		<td><span id="elh_wp_posts_post_content_filtered"><?php echo $wp_posts->post_content_filtered->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_content_filtered->CellAttributes() ?>>
<span id="el_wp_posts_post_content_filtered" class="control-group">
<span<?php echo $wp_posts->post_content_filtered->ViewAttributes() ?>>
<?php echo $wp_posts->post_content_filtered->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_parent->Visible) { // post_parent ?>
	<tr id="r_post_parent">
		<td><span id="elh_wp_posts_post_parent"><?php echo $wp_posts->post_parent->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_parent->CellAttributes() ?>>
<span id="el_wp_posts_post_parent" class="control-group">
<span<?php echo $wp_posts->post_parent->ViewAttributes() ?>>
<?php echo $wp_posts->post_parent->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->guid->Visible) { // guid ?>
	<tr id="r_guid">
		<td><span id="elh_wp_posts_guid"><?php echo $wp_posts->guid->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->guid->CellAttributes() ?>>
<span id="el_wp_posts_guid" class="control-group">
<span<?php echo $wp_posts->guid->ViewAttributes() ?>>
<?php echo $wp_posts->guid->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->menu_order->Visible) { // menu_order ?>
	<tr id="r_menu_order">
		<td><span id="elh_wp_posts_menu_order"><?php echo $wp_posts->menu_order->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->menu_order->CellAttributes() ?>>
<span id="el_wp_posts_menu_order" class="control-group">
<span<?php echo $wp_posts->menu_order->ViewAttributes() ?>>
<?php echo $wp_posts->menu_order->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_type->Visible) { // post_type ?>
	<tr id="r_post_type">
		<td><span id="elh_wp_posts_post_type"><?php echo $wp_posts->post_type->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_type->CellAttributes() ?>>
<span id="el_wp_posts_post_type" class="control-group">
<span<?php echo $wp_posts->post_type->ViewAttributes() ?>>
<?php echo $wp_posts->post_type->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_mime_type->Visible) { // post_mime_type ?>
	<tr id="r_post_mime_type">
		<td><span id="elh_wp_posts_post_mime_type"><?php echo $wp_posts->post_mime_type->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->post_mime_type->CellAttributes() ?>>
<span id="el_wp_posts_post_mime_type" class="control-group">
<span<?php echo $wp_posts->post_mime_type->ViewAttributes() ?>>
<?php echo $wp_posts->post_mime_type->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
<?php if ($wp_posts->comment_count->Visible) { // comment_count ?>
	<tr id="r_comment_count">
		<td><span id="elh_wp_posts_comment_count"><?php echo $wp_posts->comment_count->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->comment_count->CellAttributes() ?>>
<span id="el_wp_posts_comment_count" class="control-group">
<span<?php echo $wp_posts->comment_count->ViewAttributes() ?>>
<?php echo $wp_posts->comment_count->ViewValue ?></span>
</span>
</td>
	</tr>
<?php } ?>
</table>
</td></tr></table>
</form>
<script type="text/javascript">
fwp_postsview.Init();
</script>
<?php
$wp_posts_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$wp_posts_view->Page_Terminate();
?>
