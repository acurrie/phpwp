<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg10.php" ?>
<?php include_once "ewmysql10.php" ?>
<?php include_once "phpfn10.php" ?>
<?php include_once "wp_postsinfo.php" ?>
<?php include_once "userfn10.php" ?>
<?php

//
// Page class
//

$wp_posts_edit = NULL; // Initialize page object first

class cwp_posts_edit extends cwp_posts {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{9787E536-E7C1-412B-A7C2-084F8BF0B798}";

	// Table name
	var $TableName = 'wp_posts';

	// Page object name
	var $PageObjName = 'wp_posts_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sMessage . "</div>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sWarningMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sWarningMessage;
			$html .= "<div class=\"alert alert-warning ewWarning\">" . $sWarningMessage . "</div>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sSuccessMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sSuccessMessage;
			$html .= "<div class=\"alert alert-success ewSuccess\">" . $sSuccessMessage . "</div>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			if (!$hidden)
				$sErrorMessage = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>" . $sErrorMessage;
			$html .= "<div class=\"alert alert-error ewError\">" . $sErrorMessage . "</div>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<table class=\"ewStdTable\"><tr><td><div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div></td></tr></table>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p>" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Footer exists, display
			echo "<p>" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language;
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (wp_posts)
		if (!isset($GLOBALS["wp_posts"])) {
			$GLOBALS["wp_posts"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["wp_posts"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'wp_posts', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"]; // Set up curent action
		$this->ID->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["ID"] <> "") {
			$this->ID->setQueryStringValue($_GET["ID"]);
		}

		// Set up Breadcrumb
		$this->SetupBreadcrumb();

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->ID->CurrentValue == "")
			$this->Page_Terminate("wp_postslist.php"); // Invalid key, return to list

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("wp_postslist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					$sReturnUrl = $this->getReturnUrl();
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm;

		// Get upload data
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->ID->FldIsDetailKey)
			$this->ID->setFormValue($objForm->GetValue("x_ID"));
		if (!$this->post_author->FldIsDetailKey) {
			$this->post_author->setFormValue($objForm->GetValue("x_post_author"));
		}
		if (!$this->post_date->FldIsDetailKey) {
			$this->post_date->setFormValue($objForm->GetValue("x_post_date"));
			$this->post_date->CurrentValue = ew_UnFormatDateTime($this->post_date->CurrentValue, 5);
		}
		if (!$this->post_date_gmt->FldIsDetailKey) {
			$this->post_date_gmt->setFormValue($objForm->GetValue("x_post_date_gmt"));
			$this->post_date_gmt->CurrentValue = ew_UnFormatDateTime($this->post_date_gmt->CurrentValue, 5);
		}
		if (!$this->post_content->FldIsDetailKey) {
			$this->post_content->setFormValue($objForm->GetValue("x_post_content"));
		}
		if (!$this->post_title->FldIsDetailKey) {
			$this->post_title->setFormValue($objForm->GetValue("x_post_title"));
		}
		if (!$this->post_excerpt->FldIsDetailKey) {
			$this->post_excerpt->setFormValue($objForm->GetValue("x_post_excerpt"));
		}
		if (!$this->post_status->FldIsDetailKey) {
			$this->post_status->setFormValue($objForm->GetValue("x_post_status"));
		}
		if (!$this->comment_status->FldIsDetailKey) {
			$this->comment_status->setFormValue($objForm->GetValue("x_comment_status"));
		}
		if (!$this->ping_status->FldIsDetailKey) {
			$this->ping_status->setFormValue($objForm->GetValue("x_ping_status"));
		}
		if (!$this->post_password->FldIsDetailKey) {
			$this->post_password->setFormValue($objForm->GetValue("x_post_password"));
		}
		if (!$this->post_name->FldIsDetailKey) {
			$this->post_name->setFormValue($objForm->GetValue("x_post_name"));
		}
		if (!$this->to_ping->FldIsDetailKey) {
			$this->to_ping->setFormValue($objForm->GetValue("x_to_ping"));
		}
		if (!$this->pinged->FldIsDetailKey) {
			$this->pinged->setFormValue($objForm->GetValue("x_pinged"));
		}
		if (!$this->post_modified->FldIsDetailKey) {
			$this->post_modified->setFormValue($objForm->GetValue("x_post_modified"));
			$this->post_modified->CurrentValue = ew_UnFormatDateTime($this->post_modified->CurrentValue, 5);
		}
		if (!$this->post_modified_gmt->FldIsDetailKey) {
			$this->post_modified_gmt->setFormValue($objForm->GetValue("x_post_modified_gmt"));
			$this->post_modified_gmt->CurrentValue = ew_UnFormatDateTime($this->post_modified_gmt->CurrentValue, 5);
		}
		if (!$this->post_content_filtered->FldIsDetailKey) {
			$this->post_content_filtered->setFormValue($objForm->GetValue("x_post_content_filtered"));
		}
		if (!$this->post_parent->FldIsDetailKey) {
			$this->post_parent->setFormValue($objForm->GetValue("x_post_parent"));
		}
		if (!$this->guid->FldIsDetailKey) {
			$this->guid->setFormValue($objForm->GetValue("x_guid"));
		}
		if (!$this->menu_order->FldIsDetailKey) {
			$this->menu_order->setFormValue($objForm->GetValue("x_menu_order"));
		}
		if (!$this->post_type->FldIsDetailKey) {
			$this->post_type->setFormValue($objForm->GetValue("x_post_type"));
		}
		if (!$this->post_mime_type->FldIsDetailKey) {
			$this->post_mime_type->setFormValue($objForm->GetValue("x_post_mime_type"));
		}
		if (!$this->comment_count->FldIsDetailKey) {
			$this->comment_count->setFormValue($objForm->GetValue("x_comment_count"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->ID->CurrentValue = $this->ID->FormValue;
		$this->post_author->CurrentValue = $this->post_author->FormValue;
		$this->post_date->CurrentValue = $this->post_date->FormValue;
		$this->post_date->CurrentValue = ew_UnFormatDateTime($this->post_date->CurrentValue, 5);
		$this->post_date_gmt->CurrentValue = $this->post_date_gmt->FormValue;
		$this->post_date_gmt->CurrentValue = ew_UnFormatDateTime($this->post_date_gmt->CurrentValue, 5);
		$this->post_content->CurrentValue = $this->post_content->FormValue;
		$this->post_title->CurrentValue = $this->post_title->FormValue;
		$this->post_excerpt->CurrentValue = $this->post_excerpt->FormValue;
		$this->post_status->CurrentValue = $this->post_status->FormValue;
		$this->comment_status->CurrentValue = $this->comment_status->FormValue;
		$this->ping_status->CurrentValue = $this->ping_status->FormValue;
		$this->post_password->CurrentValue = $this->post_password->FormValue;
		$this->post_name->CurrentValue = $this->post_name->FormValue;
		$this->to_ping->CurrentValue = $this->to_ping->FormValue;
		$this->pinged->CurrentValue = $this->pinged->FormValue;
		$this->post_modified->CurrentValue = $this->post_modified->FormValue;
		$this->post_modified->CurrentValue = ew_UnFormatDateTime($this->post_modified->CurrentValue, 5);
		$this->post_modified_gmt->CurrentValue = $this->post_modified_gmt->FormValue;
		$this->post_modified_gmt->CurrentValue = ew_UnFormatDateTime($this->post_modified_gmt->CurrentValue, 5);
		$this->post_content_filtered->CurrentValue = $this->post_content_filtered->FormValue;
		$this->post_parent->CurrentValue = $this->post_parent->FormValue;
		$this->guid->CurrentValue = $this->guid->FormValue;
		$this->menu_order->CurrentValue = $this->menu_order->FormValue;
		$this->post_type->CurrentValue = $this->post_type->FormValue;
		$this->post_mime_type->CurrentValue = $this->post_mime_type->FormValue;
		$this->comment_count->CurrentValue = $this->comment_count->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->ID->setDbValue($rs->fields('ID'));
		$this->post_author->setDbValue($rs->fields('post_author'));
		$this->post_date->setDbValue($rs->fields('post_date'));
		$this->post_date_gmt->setDbValue($rs->fields('post_date_gmt'));
		$this->post_content->setDbValue($rs->fields('post_content'));
		$this->post_title->setDbValue($rs->fields('post_title'));
		$this->post_excerpt->setDbValue($rs->fields('post_excerpt'));
		$this->post_status->setDbValue($rs->fields('post_status'));
		$this->comment_status->setDbValue($rs->fields('comment_status'));
		$this->ping_status->setDbValue($rs->fields('ping_status'));
		$this->post_password->setDbValue($rs->fields('post_password'));
		$this->post_name->setDbValue($rs->fields('post_name'));
		$this->to_ping->setDbValue($rs->fields('to_ping'));
		$this->pinged->setDbValue($rs->fields('pinged'));
		$this->post_modified->setDbValue($rs->fields('post_modified'));
		$this->post_modified_gmt->setDbValue($rs->fields('post_modified_gmt'));
		$this->post_content_filtered->setDbValue($rs->fields('post_content_filtered'));
		$this->post_parent->setDbValue($rs->fields('post_parent'));
		$this->guid->setDbValue($rs->fields('guid'));
		$this->menu_order->setDbValue($rs->fields('menu_order'));
		$this->post_type->setDbValue($rs->fields('post_type'));
		$this->post_mime_type->setDbValue($rs->fields('post_mime_type'));
		$this->comment_count->setDbValue($rs->fields('comment_count'));
	}

	// Load DbValue from recordset
	function LoadDbValues(&$rs) {
		if (!$rs || !is_array($rs) && $rs->EOF) return;
		$row = is_array($rs) ? $rs : $rs->fields;
		$this->ID->DbValue = $row['ID'];
		$this->post_author->DbValue = $row['post_author'];
		$this->post_date->DbValue = $row['post_date'];
		$this->post_date_gmt->DbValue = $row['post_date_gmt'];
		$this->post_content->DbValue = $row['post_content'];
		$this->post_title->DbValue = $row['post_title'];
		$this->post_excerpt->DbValue = $row['post_excerpt'];
		$this->post_status->DbValue = $row['post_status'];
		$this->comment_status->DbValue = $row['comment_status'];
		$this->ping_status->DbValue = $row['ping_status'];
		$this->post_password->DbValue = $row['post_password'];
		$this->post_name->DbValue = $row['post_name'];
		$this->to_ping->DbValue = $row['to_ping'];
		$this->pinged->DbValue = $row['pinged'];
		$this->post_modified->DbValue = $row['post_modified'];
		$this->post_modified_gmt->DbValue = $row['post_modified_gmt'];
		$this->post_content_filtered->DbValue = $row['post_content_filtered'];
		$this->post_parent->DbValue = $row['post_parent'];
		$this->guid->DbValue = $row['guid'];
		$this->menu_order->DbValue = $row['menu_order'];
		$this->post_type->DbValue = $row['post_type'];
		$this->post_mime_type->DbValue = $row['post_mime_type'];
		$this->comment_count->DbValue = $row['comment_count'];
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// ID
		// post_author
		// post_date
		// post_date_gmt
		// post_content
		// post_title
		// post_excerpt
		// post_status
		// comment_status
		// ping_status
		// post_password
		// post_name
		// to_ping
		// pinged
		// post_modified
		// post_modified_gmt
		// post_content_filtered
		// post_parent
		// guid
		// menu_order
		// post_type
		// post_mime_type
		// comment_count

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// ID
			$this->ID->ViewValue = $this->ID->CurrentValue;
			$this->ID->ViewCustomAttributes = "";

			// post_author
			$this->post_author->ViewValue = $this->post_author->CurrentValue;
			$this->post_author->ViewCustomAttributes = "";

			// post_date
			$this->post_date->ViewValue = $this->post_date->CurrentValue;
			$this->post_date->ViewValue = ew_FormatDateTime($this->post_date->ViewValue, 5);
			$this->post_date->ViewCustomAttributes = "";

			// post_date_gmt
			$this->post_date_gmt->ViewValue = $this->post_date_gmt->CurrentValue;
			$this->post_date_gmt->ViewValue = ew_FormatDateTime($this->post_date_gmt->ViewValue, 5);
			$this->post_date_gmt->ViewCustomAttributes = "";

			// post_content
			$this->post_content->ViewValue = $this->post_content->CurrentValue;
			$this->post_content->ViewCustomAttributes = "";

			// post_title
			$this->post_title->ViewValue = $this->post_title->CurrentValue;
			$this->post_title->ViewCustomAttributes = "";

			// post_excerpt
			$this->post_excerpt->ViewValue = $this->post_excerpt->CurrentValue;
			$this->post_excerpt->ViewCustomAttributes = "";

			// post_status
			$this->post_status->ViewValue = $this->post_status->CurrentValue;
			$this->post_status->ViewCustomAttributes = "";

			// comment_status
			$this->comment_status->ViewValue = $this->comment_status->CurrentValue;
			$this->comment_status->ViewCustomAttributes = "";

			// ping_status
			$this->ping_status->ViewValue = $this->ping_status->CurrentValue;
			$this->ping_status->ViewCustomAttributes = "";

			// post_password
			$this->post_password->ViewValue = $this->post_password->CurrentValue;
			$this->post_password->ViewCustomAttributes = "";

			// post_name
			$this->post_name->ViewValue = $this->post_name->CurrentValue;
			$this->post_name->ViewCustomAttributes = "";

			// to_ping
			$this->to_ping->ViewValue = $this->to_ping->CurrentValue;
			$this->to_ping->ViewCustomAttributes = "";

			// pinged
			$this->pinged->ViewValue = $this->pinged->CurrentValue;
			$this->pinged->ViewCustomAttributes = "";

			// post_modified
			$this->post_modified->ViewValue = $this->post_modified->CurrentValue;
			$this->post_modified->ViewValue = ew_FormatDateTime($this->post_modified->ViewValue, 5);
			$this->post_modified->ViewCustomAttributes = "";

			// post_modified_gmt
			$this->post_modified_gmt->ViewValue = $this->post_modified_gmt->CurrentValue;
			$this->post_modified_gmt->ViewValue = ew_FormatDateTime($this->post_modified_gmt->ViewValue, 5);
			$this->post_modified_gmt->ViewCustomAttributes = "";

			// post_content_filtered
			$this->post_content_filtered->ViewValue = $this->post_content_filtered->CurrentValue;
			$this->post_content_filtered->ViewCustomAttributes = "";

			// post_parent
			$this->post_parent->ViewValue = $this->post_parent->CurrentValue;
			$this->post_parent->ViewCustomAttributes = "";

			// guid
			$this->guid->ViewValue = $this->guid->CurrentValue;
			$this->guid->ViewCustomAttributes = "";

			// menu_order
			$this->menu_order->ViewValue = $this->menu_order->CurrentValue;
			$this->menu_order->ViewCustomAttributes = "";

			// post_type
			$this->post_type->ViewValue = $this->post_type->CurrentValue;
			$this->post_type->ViewCustomAttributes = "";

			// post_mime_type
			$this->post_mime_type->ViewValue = $this->post_mime_type->CurrentValue;
			$this->post_mime_type->ViewCustomAttributes = "";

			// comment_count
			$this->comment_count->ViewValue = $this->comment_count->CurrentValue;
			$this->comment_count->ViewCustomAttributes = "";

			// ID
			$this->ID->LinkCustomAttributes = "";
			$this->ID->HrefValue = "";
			$this->ID->TooltipValue = "";

			// post_author
			$this->post_author->LinkCustomAttributes = "";
			$this->post_author->HrefValue = "";
			$this->post_author->TooltipValue = "";

			// post_date
			$this->post_date->LinkCustomAttributes = "";
			$this->post_date->HrefValue = "";
			$this->post_date->TooltipValue = "";

			// post_date_gmt
			$this->post_date_gmt->LinkCustomAttributes = "";
			$this->post_date_gmt->HrefValue = "";
			$this->post_date_gmt->TooltipValue = "";

			// post_content
			$this->post_content->LinkCustomAttributes = "";
			$this->post_content->HrefValue = "";
			$this->post_content->TooltipValue = "";

			// post_title
			$this->post_title->LinkCustomAttributes = "";
			$this->post_title->HrefValue = "";
			$this->post_title->TooltipValue = "";

			// post_excerpt
			$this->post_excerpt->LinkCustomAttributes = "";
			$this->post_excerpt->HrefValue = "";
			$this->post_excerpt->TooltipValue = "";

			// post_status
			$this->post_status->LinkCustomAttributes = "";
			$this->post_status->HrefValue = "";
			$this->post_status->TooltipValue = "";

			// comment_status
			$this->comment_status->LinkCustomAttributes = "";
			$this->comment_status->HrefValue = "";
			$this->comment_status->TooltipValue = "";

			// ping_status
			$this->ping_status->LinkCustomAttributes = "";
			$this->ping_status->HrefValue = "";
			$this->ping_status->TooltipValue = "";

			// post_password
			$this->post_password->LinkCustomAttributes = "";
			$this->post_password->HrefValue = "";
			$this->post_password->TooltipValue = "";

			// post_name
			$this->post_name->LinkCustomAttributes = "";
			$this->post_name->HrefValue = "";
			$this->post_name->TooltipValue = "";

			// to_ping
			$this->to_ping->LinkCustomAttributes = "";
			$this->to_ping->HrefValue = "";
			$this->to_ping->TooltipValue = "";

			// pinged
			$this->pinged->LinkCustomAttributes = "";
			$this->pinged->HrefValue = "";
			$this->pinged->TooltipValue = "";

			// post_modified
			$this->post_modified->LinkCustomAttributes = "";
			$this->post_modified->HrefValue = "";
			$this->post_modified->TooltipValue = "";

			// post_modified_gmt
			$this->post_modified_gmt->LinkCustomAttributes = "";
			$this->post_modified_gmt->HrefValue = "";
			$this->post_modified_gmt->TooltipValue = "";

			// post_content_filtered
			$this->post_content_filtered->LinkCustomAttributes = "";
			$this->post_content_filtered->HrefValue = "";
			$this->post_content_filtered->TooltipValue = "";

			// post_parent
			$this->post_parent->LinkCustomAttributes = "";
			$this->post_parent->HrefValue = "";
			$this->post_parent->TooltipValue = "";

			// guid
			$this->guid->LinkCustomAttributes = "";
			$this->guid->HrefValue = "";
			$this->guid->TooltipValue = "";

			// menu_order
			$this->menu_order->LinkCustomAttributes = "";
			$this->menu_order->HrefValue = "";
			$this->menu_order->TooltipValue = "";

			// post_type
			$this->post_type->LinkCustomAttributes = "";
			$this->post_type->HrefValue = "";
			$this->post_type->TooltipValue = "";

			// post_mime_type
			$this->post_mime_type->LinkCustomAttributes = "";
			$this->post_mime_type->HrefValue = "";
			$this->post_mime_type->TooltipValue = "";

			// comment_count
			$this->comment_count->LinkCustomAttributes = "";
			$this->comment_count->HrefValue = "";
			$this->comment_count->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// ID
			$this->ID->EditCustomAttributes = "";
			$this->ID->EditValue = $this->ID->CurrentValue;
			$this->ID->ViewCustomAttributes = "";

			// post_author
			$this->post_author->EditCustomAttributes = "";
			$this->post_author->EditValue = ew_HtmlEncode($this->post_author->CurrentValue);
			$this->post_author->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_author->FldCaption()));

			// post_date
			$this->post_date->EditCustomAttributes = "";
			$this->post_date->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->post_date->CurrentValue, 5));
			$this->post_date->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_date->FldCaption()));

			// post_date_gmt
			$this->post_date_gmt->EditCustomAttributes = "";
			$this->post_date_gmt->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->post_date_gmt->CurrentValue, 5));
			$this->post_date_gmt->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_date_gmt->FldCaption()));

			// post_content
			$this->post_content->EditCustomAttributes = "";
			$this->post_content->EditValue = $this->post_content->CurrentValue;
			$this->post_content->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_content->FldCaption()));

			// post_title
			$this->post_title->EditCustomAttributes = "";
			$this->post_title->EditValue = $this->post_title->CurrentValue;
			$this->post_title->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_title->FldCaption()));

			// post_excerpt
			$this->post_excerpt->EditCustomAttributes = "";
			$this->post_excerpt->EditValue = $this->post_excerpt->CurrentValue;
			$this->post_excerpt->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_excerpt->FldCaption()));

			// post_status
			$this->post_status->EditCustomAttributes = "";
			$this->post_status->EditValue = ew_HtmlEncode($this->post_status->CurrentValue);
			$this->post_status->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_status->FldCaption()));

			// comment_status
			$this->comment_status->EditCustomAttributes = "";
			$this->comment_status->EditValue = ew_HtmlEncode($this->comment_status->CurrentValue);
			$this->comment_status->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->comment_status->FldCaption()));

			// ping_status
			$this->ping_status->EditCustomAttributes = "";
			$this->ping_status->EditValue = ew_HtmlEncode($this->ping_status->CurrentValue);
			$this->ping_status->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->ping_status->FldCaption()));

			// post_password
			$this->post_password->EditCustomAttributes = "";
			$this->post_password->EditValue = ew_HtmlEncode($this->post_password->CurrentValue);
			$this->post_password->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_password->FldCaption()));

			// post_name
			$this->post_name->EditCustomAttributes = "";
			$this->post_name->EditValue = ew_HtmlEncode($this->post_name->CurrentValue);
			$this->post_name->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_name->FldCaption()));

			// to_ping
			$this->to_ping->EditCustomAttributes = "";
			$this->to_ping->EditValue = $this->to_ping->CurrentValue;
			$this->to_ping->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->to_ping->FldCaption()));

			// pinged
			$this->pinged->EditCustomAttributes = "";
			$this->pinged->EditValue = $this->pinged->CurrentValue;
			$this->pinged->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->pinged->FldCaption()));

			// post_modified
			$this->post_modified->EditCustomAttributes = "";
			$this->post_modified->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->post_modified->CurrentValue, 5));
			$this->post_modified->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_modified->FldCaption()));

			// post_modified_gmt
			$this->post_modified_gmt->EditCustomAttributes = "";
			$this->post_modified_gmt->EditValue = ew_HtmlEncode(ew_FormatDateTime($this->post_modified_gmt->CurrentValue, 5));
			$this->post_modified_gmt->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_modified_gmt->FldCaption()));

			// post_content_filtered
			$this->post_content_filtered->EditCustomAttributes = "";
			$this->post_content_filtered->EditValue = $this->post_content_filtered->CurrentValue;
			$this->post_content_filtered->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_content_filtered->FldCaption()));

			// post_parent
			$this->post_parent->EditCustomAttributes = "";
			$this->post_parent->EditValue = ew_HtmlEncode($this->post_parent->CurrentValue);
			$this->post_parent->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_parent->FldCaption()));

			// guid
			$this->guid->EditCustomAttributes = "";
			$this->guid->EditValue = ew_HtmlEncode($this->guid->CurrentValue);
			$this->guid->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->guid->FldCaption()));

			// menu_order
			$this->menu_order->EditCustomAttributes = "";
			$this->menu_order->EditValue = ew_HtmlEncode($this->menu_order->CurrentValue);
			$this->menu_order->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->menu_order->FldCaption()));

			// post_type
			$this->post_type->EditCustomAttributes = "";
			$this->post_type->EditValue = ew_HtmlEncode($this->post_type->CurrentValue);
			$this->post_type->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_type->FldCaption()));

			// post_mime_type
			$this->post_mime_type->EditCustomAttributes = "";
			$this->post_mime_type->EditValue = ew_HtmlEncode($this->post_mime_type->CurrentValue);
			$this->post_mime_type->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->post_mime_type->FldCaption()));

			// comment_count
			$this->comment_count->EditCustomAttributes = "";
			$this->comment_count->EditValue = ew_HtmlEncode($this->comment_count->CurrentValue);
			$this->comment_count->PlaceHolder = ew_HtmlEncode(ew_RemoveHtml($this->comment_count->FldCaption()));

			// Edit refer script
			// ID

			$this->ID->HrefValue = "";

			// post_author
			$this->post_author->HrefValue = "";

			// post_date
			$this->post_date->HrefValue = "";

			// post_date_gmt
			$this->post_date_gmt->HrefValue = "";

			// post_content
			$this->post_content->HrefValue = "";

			// post_title
			$this->post_title->HrefValue = "";

			// post_excerpt
			$this->post_excerpt->HrefValue = "";

			// post_status
			$this->post_status->HrefValue = "";

			// comment_status
			$this->comment_status->HrefValue = "";

			// ping_status
			$this->ping_status->HrefValue = "";

			// post_password
			$this->post_password->HrefValue = "";

			// post_name
			$this->post_name->HrefValue = "";

			// to_ping
			$this->to_ping->HrefValue = "";

			// pinged
			$this->pinged->HrefValue = "";

			// post_modified
			$this->post_modified->HrefValue = "";

			// post_modified_gmt
			$this->post_modified_gmt->HrefValue = "";

			// post_content_filtered
			$this->post_content_filtered->HrefValue = "";

			// post_parent
			$this->post_parent->HrefValue = "";

			// guid
			$this->guid->HrefValue = "";

			// menu_order
			$this->menu_order->HrefValue = "";

			// post_type
			$this->post_type->HrefValue = "";

			// post_mime_type
			$this->post_mime_type->HrefValue = "";

			// comment_count
			$this->comment_count->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!$this->post_author->FldIsDetailKey && !is_null($this->post_author->FormValue) && $this->post_author->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_author->FldCaption());
		}
		if (!ew_CheckInteger($this->post_author->FormValue)) {
			ew_AddMessage($gsFormError, $this->post_author->FldErrMsg());
		}
		if (!$this->post_date->FldIsDetailKey && !is_null($this->post_date->FormValue) && $this->post_date->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_date->FldCaption());
		}
		if (!ew_CheckDate($this->post_date->FormValue)) {
			ew_AddMessage($gsFormError, $this->post_date->FldErrMsg());
		}
		if (!$this->post_date_gmt->FldIsDetailKey && !is_null($this->post_date_gmt->FormValue) && $this->post_date_gmt->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_date_gmt->FldCaption());
		}
		if (!ew_CheckDate($this->post_date_gmt->FormValue)) {
			ew_AddMessage($gsFormError, $this->post_date_gmt->FldErrMsg());
		}
		if (!$this->post_content->FldIsDetailKey && !is_null($this->post_content->FormValue) && $this->post_content->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_content->FldCaption());
		}
		if (!$this->post_title->FldIsDetailKey && !is_null($this->post_title->FormValue) && $this->post_title->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_title->FldCaption());
		}
		if (!$this->post_excerpt->FldIsDetailKey && !is_null($this->post_excerpt->FormValue) && $this->post_excerpt->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_excerpt->FldCaption());
		}
		if (!$this->post_status->FldIsDetailKey && !is_null($this->post_status->FormValue) && $this->post_status->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_status->FldCaption());
		}
		if (!$this->comment_status->FldIsDetailKey && !is_null($this->comment_status->FormValue) && $this->comment_status->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->comment_status->FldCaption());
		}
		if (!$this->ping_status->FldIsDetailKey && !is_null($this->ping_status->FormValue) && $this->ping_status->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->ping_status->FldCaption());
		}
		if (!$this->post_password->FldIsDetailKey && !is_null($this->post_password->FormValue) && $this->post_password->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_password->FldCaption());
		}
		if (!$this->post_name->FldIsDetailKey && !is_null($this->post_name->FormValue) && $this->post_name->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_name->FldCaption());
		}
		if (!$this->to_ping->FldIsDetailKey && !is_null($this->to_ping->FormValue) && $this->to_ping->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->to_ping->FldCaption());
		}
		if (!$this->pinged->FldIsDetailKey && !is_null($this->pinged->FormValue) && $this->pinged->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->pinged->FldCaption());
		}
		if (!$this->post_modified->FldIsDetailKey && !is_null($this->post_modified->FormValue) && $this->post_modified->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_modified->FldCaption());
		}
		if (!ew_CheckDate($this->post_modified->FormValue)) {
			ew_AddMessage($gsFormError, $this->post_modified->FldErrMsg());
		}
		if (!$this->post_modified_gmt->FldIsDetailKey && !is_null($this->post_modified_gmt->FormValue) && $this->post_modified_gmt->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_modified_gmt->FldCaption());
		}
		if (!ew_CheckDate($this->post_modified_gmt->FormValue)) {
			ew_AddMessage($gsFormError, $this->post_modified_gmt->FldErrMsg());
		}
		if (!$this->post_content_filtered->FldIsDetailKey && !is_null($this->post_content_filtered->FormValue) && $this->post_content_filtered->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_content_filtered->FldCaption());
		}
		if (!$this->post_parent->FldIsDetailKey && !is_null($this->post_parent->FormValue) && $this->post_parent->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_parent->FldCaption());
		}
		if (!ew_CheckInteger($this->post_parent->FormValue)) {
			ew_AddMessage($gsFormError, $this->post_parent->FldErrMsg());
		}
		if (!$this->guid->FldIsDetailKey && !is_null($this->guid->FormValue) && $this->guid->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->guid->FldCaption());
		}
		if (!$this->menu_order->FldIsDetailKey && !is_null($this->menu_order->FormValue) && $this->menu_order->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->menu_order->FldCaption());
		}
		if (!ew_CheckInteger($this->menu_order->FormValue)) {
			ew_AddMessage($gsFormError, $this->menu_order->FldErrMsg());
		}
		if (!$this->post_type->FldIsDetailKey && !is_null($this->post_type->FormValue) && $this->post_type->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_type->FldCaption());
		}
		if (!$this->post_mime_type->FldIsDetailKey && !is_null($this->post_mime_type->FormValue) && $this->post_mime_type->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->post_mime_type->FldCaption());
		}
		if (!$this->comment_count->FldIsDetailKey && !is_null($this->comment_count->FormValue) && $this->comment_count->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->comment_count->FldCaption());
		}
		if (!ew_CheckInteger($this->comment_count->FormValue)) {
			ew_AddMessage($gsFormError, $this->comment_count->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$this->LoadDbValues($rsold);
			$rsnew = array();

			// post_author
			$this->post_author->SetDbValueDef($rsnew, $this->post_author->CurrentValue, 0, $this->post_author->ReadOnly);

			// post_date
			$this->post_date->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->post_date->CurrentValue, 5), ew_CurrentDate(), $this->post_date->ReadOnly);

			// post_date_gmt
			$this->post_date_gmt->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->post_date_gmt->CurrentValue, 5), ew_CurrentDate(), $this->post_date_gmt->ReadOnly);

			// post_content
			$this->post_content->SetDbValueDef($rsnew, $this->post_content->CurrentValue, "", $this->post_content->ReadOnly);

			// post_title
			$this->post_title->SetDbValueDef($rsnew, $this->post_title->CurrentValue, "", $this->post_title->ReadOnly);

			// post_excerpt
			$this->post_excerpt->SetDbValueDef($rsnew, $this->post_excerpt->CurrentValue, "", $this->post_excerpt->ReadOnly);

			// post_status
			$this->post_status->SetDbValueDef($rsnew, $this->post_status->CurrentValue, "", $this->post_status->ReadOnly);

			// comment_status
			$this->comment_status->SetDbValueDef($rsnew, $this->comment_status->CurrentValue, "", $this->comment_status->ReadOnly);

			// ping_status
			$this->ping_status->SetDbValueDef($rsnew, $this->ping_status->CurrentValue, "", $this->ping_status->ReadOnly);

			// post_password
			$this->post_password->SetDbValueDef($rsnew, $this->post_password->CurrentValue, "", $this->post_password->ReadOnly);

			// post_name
			$this->post_name->SetDbValueDef($rsnew, $this->post_name->CurrentValue, "", $this->post_name->ReadOnly);

			// to_ping
			$this->to_ping->SetDbValueDef($rsnew, $this->to_ping->CurrentValue, "", $this->to_ping->ReadOnly);

			// pinged
			$this->pinged->SetDbValueDef($rsnew, $this->pinged->CurrentValue, "", $this->pinged->ReadOnly);

			// post_modified
			$this->post_modified->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->post_modified->CurrentValue, 5), ew_CurrentDate(), $this->post_modified->ReadOnly);

			// post_modified_gmt
			$this->post_modified_gmt->SetDbValueDef($rsnew, ew_UnFormatDateTime($this->post_modified_gmt->CurrentValue, 5), ew_CurrentDate(), $this->post_modified_gmt->ReadOnly);

			// post_content_filtered
			$this->post_content_filtered->SetDbValueDef($rsnew, $this->post_content_filtered->CurrentValue, "", $this->post_content_filtered->ReadOnly);

			// post_parent
			$this->post_parent->SetDbValueDef($rsnew, $this->post_parent->CurrentValue, 0, $this->post_parent->ReadOnly);

			// guid
			$this->guid->SetDbValueDef($rsnew, $this->guid->CurrentValue, "", $this->guid->ReadOnly);

			// menu_order
			$this->menu_order->SetDbValueDef($rsnew, $this->menu_order->CurrentValue, 0, $this->menu_order->ReadOnly);

			// post_type
			$this->post_type->SetDbValueDef($rsnew, $this->post_type->CurrentValue, "", $this->post_type->ReadOnly);

			// post_mime_type
			$this->post_mime_type->SetDbValueDef($rsnew, $this->post_mime_type->CurrentValue, "", $this->post_mime_type->ReadOnly);

			// comment_count
			$this->comment_count->SetDbValueDef($rsnew, $this->comment_count->CurrentValue, 0, $this->comment_count->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = 'ew_ErrorFn';
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew, "", $rsold);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
				if ($EditRow) {
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		$rs->Close();
		return $EditRow;
	}

	// Set up Breadcrumb
	function SetupBreadcrumb() {
		global $Breadcrumb, $Language;
		$Breadcrumb = new cBreadcrumb();
		$PageCaption = $this->TableCaption();
		$Breadcrumb->Add("list", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", "wp_postslist.php", $this->TableVar);
		$PageCaption = $Language->Phrase("edit");
		$Breadcrumb->Add("edit", "<span id=\"ewPageCaption\">" . $PageCaption . "</span>", ew_CurrentUrl(), $this->TableVar);
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Render event
	function Page_Render() {

		//echo "Page Render";
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($wp_posts_edit)) $wp_posts_edit = new cwp_posts_edit();

// Page init
$wp_posts_edit->Page_Init();

// Page main
$wp_posts_edit->Page_Main();

// Global Page Rendering event (in userfn*.php)
Page_Rendering();

// Page Rendering event
$wp_posts_edit->Page_Render();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var wp_posts_edit = new ew_Page("wp_posts_edit");
wp_posts_edit.PageID = "edit"; // Page ID
var EW_PAGE_ID = wp_posts_edit.PageID; // For backward compatibility

// Form object
var fwp_postsedit = new ew_Form("fwp_postsedit");

// Validate form
fwp_postsedit.Validate = function() {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	var $ = jQuery, fobj = this.GetForm(), $fobj = $(fobj);
	this.PostAutoSuggest();
	if ($fobj.find("#a_confirm").val() == "F")
		return true;
	var elm, felm, uelm, addcnt = 0;
	var $k = $fobj.find("#" + this.FormKeyCountName); // Get key_count
	var rowcnt = ($k[0]) ? parseInt($k.val(), 10) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // Check rowcnt == 0 => Inline-Add
	var gridinsert = $fobj.find("#a_list").val() == "gridinsert";
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = ($k[0]) ? String(i) : "";
		$fobj.data("rowindex", infix);
			elm = this.GetElements("x" + infix + "_post_author");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_author->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_author");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($wp_posts->post_author->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_post_date");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_date->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_date");
			if (elm && !ew_CheckDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($wp_posts->post_date->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_post_date_gmt");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_date_gmt->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_date_gmt");
			if (elm && !ew_CheckDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($wp_posts->post_date_gmt->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_post_content");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_content->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_title");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_title->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_excerpt");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_excerpt->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_status");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_status->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_comment_status");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->comment_status->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_ping_status");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->ping_status->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_password");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_password->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_name");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_name->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_to_ping");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->to_ping->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_pinged");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->pinged->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_modified");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_modified->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_modified");
			if (elm && !ew_CheckDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($wp_posts->post_modified->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_post_modified_gmt");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_modified_gmt->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_modified_gmt");
			if (elm && !ew_CheckDate(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($wp_posts->post_modified_gmt->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_post_content_filtered");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_content_filtered->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_parent");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_parent->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_parent");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($wp_posts->post_parent->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_guid");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->guid->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_menu_order");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->menu_order->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_menu_order");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($wp_posts->menu_order->FldErrMsg()) ?>");
			elm = this.GetElements("x" + infix + "_post_type");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_type->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_post_mime_type");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->post_mime_type->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_comment_count");
			if (elm && !ew_HasValue(elm))
				return this.OnError(elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($wp_posts->comment_count->FldCaption()) ?>");
			elm = this.GetElements("x" + infix + "_comment_count");
			if (elm && !ew_CheckInteger(elm.value))
				return this.OnError(elm, "<?php echo ew_JsEncode2($wp_posts->comment_count->FldErrMsg()) ?>");

			// Set up row object
			ew_ElementsToRow(fobj);

			// Fire Form_CustomValidate event
			if (!this.Form_CustomValidate(fobj))
				return false;
	}

	// Process detail forms
	var dfs = $fobj.find("input[name='detailpage']").get();
	for (var i = 0; i < dfs.length; i++) {
		var df = dfs[i], val = df.value;
		if (val && ewForms[val])
			if (!ewForms[val].Validate())
				return false;
	}
	return true;
}

// Form_CustomValidate event
fwp_postsedit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fwp_postsedit.ValidateRequired = true;
<?php } else { ?>
fwp_postsedit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php $Breadcrumb->Render(); ?>
<?php $wp_posts_edit->ShowPageHeader(); ?>
<?php
$wp_posts_edit->ShowMessage();
?>
<form name="fwp_postsedit" id="fwp_postsedit" class="ewForm form-horizontal" action="<?php echo ew_CurrentPage() ?>" method="post">
<input type="hidden" name="t" value="wp_posts">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<table cellspacing="0" class="ewGrid"><tr><td>
<table id="tbl_wp_postsedit" class="table table-bordered table-striped">
<?php if ($wp_posts->ID->Visible) { // ID ?>
	<tr id="r_ID">
		<td><span id="elh_wp_posts_ID"><?php echo $wp_posts->ID->FldCaption() ?></span></td>
		<td<?php echo $wp_posts->ID->CellAttributes() ?>>
<span id="el_wp_posts_ID" class="control-group">
<span<?php echo $wp_posts->ID->ViewAttributes() ?>>
<?php echo $wp_posts->ID->EditValue ?></span>
</span>
<input type="hidden" data-field="x_ID" name="x_ID" id="x_ID" value="<?php echo ew_HtmlEncode($wp_posts->ID->CurrentValue) ?>">
<?php echo $wp_posts->ID->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_author->Visible) { // post_author ?>
	<tr id="r_post_author">
		<td><span id="elh_wp_posts_post_author"><?php echo $wp_posts->post_author->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_author->CellAttributes() ?>>
<span id="el_wp_posts_post_author" class="control-group">
<input type="text" data-field="x_post_author" name="x_post_author" id="x_post_author" size="30" placeholder="<?php echo $wp_posts->post_author->PlaceHolder ?>" value="<?php echo $wp_posts->post_author->EditValue ?>"<?php echo $wp_posts->post_author->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_author->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_date->Visible) { // post_date ?>
	<tr id="r_post_date">
		<td><span id="elh_wp_posts_post_date"><?php echo $wp_posts->post_date->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_date->CellAttributes() ?>>
<span id="el_wp_posts_post_date" class="control-group">
<input type="text" data-field="x_post_date" name="x_post_date" id="x_post_date" placeholder="<?php echo $wp_posts->post_date->PlaceHolder ?>" value="<?php echo $wp_posts->post_date->EditValue ?>"<?php echo $wp_posts->post_date->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_date->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_date_gmt->Visible) { // post_date_gmt ?>
	<tr id="r_post_date_gmt">
		<td><span id="elh_wp_posts_post_date_gmt"><?php echo $wp_posts->post_date_gmt->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_date_gmt->CellAttributes() ?>>
<span id="el_wp_posts_post_date_gmt" class="control-group">
<input type="text" data-field="x_post_date_gmt" name="x_post_date_gmt" id="x_post_date_gmt" placeholder="<?php echo $wp_posts->post_date_gmt->PlaceHolder ?>" value="<?php echo $wp_posts->post_date_gmt->EditValue ?>"<?php echo $wp_posts->post_date_gmt->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_date_gmt->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_content->Visible) { // post_content ?>
	<tr id="r_post_content">
		<td><span id="elh_wp_posts_post_content"><?php echo $wp_posts->post_content->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_content->CellAttributes() ?>>
<span id="el_wp_posts_post_content" class="control-group">
<textarea data-field="x_post_content" name="x_post_content" id="x_post_content" cols="35" rows="4" placeholder="<?php echo $wp_posts->post_content->PlaceHolder ?>"<?php echo $wp_posts->post_content->EditAttributes() ?>><?php echo $wp_posts->post_content->EditValue ?></textarea>
</span>
<?php echo $wp_posts->post_content->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_title->Visible) { // post_title ?>
	<tr id="r_post_title">
		<td><span id="elh_wp_posts_post_title"><?php echo $wp_posts->post_title->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_title->CellAttributes() ?>>
<span id="el_wp_posts_post_title" class="control-group">
<textarea data-field="x_post_title" name="x_post_title" id="x_post_title" cols="35" rows="4" placeholder="<?php echo $wp_posts->post_title->PlaceHolder ?>"<?php echo $wp_posts->post_title->EditAttributes() ?>><?php echo $wp_posts->post_title->EditValue ?></textarea>
</span>
<?php echo $wp_posts->post_title->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_excerpt->Visible) { // post_excerpt ?>
	<tr id="r_post_excerpt">
		<td><span id="elh_wp_posts_post_excerpt"><?php echo $wp_posts->post_excerpt->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_excerpt->CellAttributes() ?>>
<span id="el_wp_posts_post_excerpt" class="control-group">
<textarea data-field="x_post_excerpt" name="x_post_excerpt" id="x_post_excerpt" cols="35" rows="4" placeholder="<?php echo $wp_posts->post_excerpt->PlaceHolder ?>"<?php echo $wp_posts->post_excerpt->EditAttributes() ?>><?php echo $wp_posts->post_excerpt->EditValue ?></textarea>
</span>
<?php echo $wp_posts->post_excerpt->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_status->Visible) { // post_status ?>
	<tr id="r_post_status">
		<td><span id="elh_wp_posts_post_status"><?php echo $wp_posts->post_status->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_status->CellAttributes() ?>>
<span id="el_wp_posts_post_status" class="control-group">
<input type="text" data-field="x_post_status" name="x_post_status" id="x_post_status" size="30" maxlength="20" placeholder="<?php echo $wp_posts->post_status->PlaceHolder ?>" value="<?php echo $wp_posts->post_status->EditValue ?>"<?php echo $wp_posts->post_status->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_status->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->comment_status->Visible) { // comment_status ?>
	<tr id="r_comment_status">
		<td><span id="elh_wp_posts_comment_status"><?php echo $wp_posts->comment_status->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->comment_status->CellAttributes() ?>>
<span id="el_wp_posts_comment_status" class="control-group">
<input type="text" data-field="x_comment_status" name="x_comment_status" id="x_comment_status" size="30" maxlength="20" placeholder="<?php echo $wp_posts->comment_status->PlaceHolder ?>" value="<?php echo $wp_posts->comment_status->EditValue ?>"<?php echo $wp_posts->comment_status->EditAttributes() ?>>
</span>
<?php echo $wp_posts->comment_status->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->ping_status->Visible) { // ping_status ?>
	<tr id="r_ping_status">
		<td><span id="elh_wp_posts_ping_status"><?php echo $wp_posts->ping_status->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->ping_status->CellAttributes() ?>>
<span id="el_wp_posts_ping_status" class="control-group">
<input type="text" data-field="x_ping_status" name="x_ping_status" id="x_ping_status" size="30" maxlength="20" placeholder="<?php echo $wp_posts->ping_status->PlaceHolder ?>" value="<?php echo $wp_posts->ping_status->EditValue ?>"<?php echo $wp_posts->ping_status->EditAttributes() ?>>
</span>
<?php echo $wp_posts->ping_status->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_password->Visible) { // post_password ?>
	<tr id="r_post_password">
		<td><span id="elh_wp_posts_post_password"><?php echo $wp_posts->post_password->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_password->CellAttributes() ?>>
<span id="el_wp_posts_post_password" class="control-group">
<input type="text" data-field="x_post_password" name="x_post_password" id="x_post_password" size="30" maxlength="255" placeholder="<?php echo $wp_posts->post_password->PlaceHolder ?>" value="<?php echo $wp_posts->post_password->EditValue ?>"<?php echo $wp_posts->post_password->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_password->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_name->Visible) { // post_name ?>
	<tr id="r_post_name">
		<td><span id="elh_wp_posts_post_name"><?php echo $wp_posts->post_name->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_name->CellAttributes() ?>>
<span id="el_wp_posts_post_name" class="control-group">
<input type="text" data-field="x_post_name" name="x_post_name" id="x_post_name" size="30" maxlength="200" placeholder="<?php echo $wp_posts->post_name->PlaceHolder ?>" value="<?php echo $wp_posts->post_name->EditValue ?>"<?php echo $wp_posts->post_name->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_name->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->to_ping->Visible) { // to_ping ?>
	<tr id="r_to_ping">
		<td><span id="elh_wp_posts_to_ping"><?php echo $wp_posts->to_ping->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->to_ping->CellAttributes() ?>>
<span id="el_wp_posts_to_ping" class="control-group">
<textarea data-field="x_to_ping" name="x_to_ping" id="x_to_ping" cols="35" rows="4" placeholder="<?php echo $wp_posts->to_ping->PlaceHolder ?>"<?php echo $wp_posts->to_ping->EditAttributes() ?>><?php echo $wp_posts->to_ping->EditValue ?></textarea>
</span>
<?php echo $wp_posts->to_ping->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->pinged->Visible) { // pinged ?>
	<tr id="r_pinged">
		<td><span id="elh_wp_posts_pinged"><?php echo $wp_posts->pinged->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->pinged->CellAttributes() ?>>
<span id="el_wp_posts_pinged" class="control-group">
<textarea data-field="x_pinged" name="x_pinged" id="x_pinged" cols="35" rows="4" placeholder="<?php echo $wp_posts->pinged->PlaceHolder ?>"<?php echo $wp_posts->pinged->EditAttributes() ?>><?php echo $wp_posts->pinged->EditValue ?></textarea>
</span>
<?php echo $wp_posts->pinged->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_modified->Visible) { // post_modified ?>
	<tr id="r_post_modified">
		<td><span id="elh_wp_posts_post_modified"><?php echo $wp_posts->post_modified->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_modified->CellAttributes() ?>>
<span id="el_wp_posts_post_modified" class="control-group">
<input type="text" data-field="x_post_modified" name="x_post_modified" id="x_post_modified" placeholder="<?php echo $wp_posts->post_modified->PlaceHolder ?>" value="<?php echo $wp_posts->post_modified->EditValue ?>"<?php echo $wp_posts->post_modified->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_modified->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_modified_gmt->Visible) { // post_modified_gmt ?>
	<tr id="r_post_modified_gmt">
		<td><span id="elh_wp_posts_post_modified_gmt"><?php echo $wp_posts->post_modified_gmt->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_modified_gmt->CellAttributes() ?>>
<span id="el_wp_posts_post_modified_gmt" class="control-group">
<input type="text" data-field="x_post_modified_gmt" name="x_post_modified_gmt" id="x_post_modified_gmt" placeholder="<?php echo $wp_posts->post_modified_gmt->PlaceHolder ?>" value="<?php echo $wp_posts->post_modified_gmt->EditValue ?>"<?php echo $wp_posts->post_modified_gmt->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_modified_gmt->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_content_filtered->Visible) { // post_content_filtered ?>
	<tr id="r_post_content_filtered">
		<td><span id="elh_wp_posts_post_content_filtered"><?php echo $wp_posts->post_content_filtered->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_content_filtered->CellAttributes() ?>>
<span id="el_wp_posts_post_content_filtered" class="control-group">
<textarea data-field="x_post_content_filtered" name="x_post_content_filtered" id="x_post_content_filtered" cols="35" rows="4" placeholder="<?php echo $wp_posts->post_content_filtered->PlaceHolder ?>"<?php echo $wp_posts->post_content_filtered->EditAttributes() ?>><?php echo $wp_posts->post_content_filtered->EditValue ?></textarea>
</span>
<?php echo $wp_posts->post_content_filtered->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_parent->Visible) { // post_parent ?>
	<tr id="r_post_parent">
		<td><span id="elh_wp_posts_post_parent"><?php echo $wp_posts->post_parent->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_parent->CellAttributes() ?>>
<span id="el_wp_posts_post_parent" class="control-group">
<input type="text" data-field="x_post_parent" name="x_post_parent" id="x_post_parent" size="30" placeholder="<?php echo $wp_posts->post_parent->PlaceHolder ?>" value="<?php echo $wp_posts->post_parent->EditValue ?>"<?php echo $wp_posts->post_parent->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_parent->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->guid->Visible) { // guid ?>
	<tr id="r_guid">
		<td><span id="elh_wp_posts_guid"><?php echo $wp_posts->guid->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->guid->CellAttributes() ?>>
<span id="el_wp_posts_guid" class="control-group">
<input type="text" data-field="x_guid" name="x_guid" id="x_guid" size="30" maxlength="255" placeholder="<?php echo $wp_posts->guid->PlaceHolder ?>" value="<?php echo $wp_posts->guid->EditValue ?>"<?php echo $wp_posts->guid->EditAttributes() ?>>
</span>
<?php echo $wp_posts->guid->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->menu_order->Visible) { // menu_order ?>
	<tr id="r_menu_order">
		<td><span id="elh_wp_posts_menu_order"><?php echo $wp_posts->menu_order->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->menu_order->CellAttributes() ?>>
<span id="el_wp_posts_menu_order" class="control-group">
<input type="text" data-field="x_menu_order" name="x_menu_order" id="x_menu_order" size="30" placeholder="<?php echo $wp_posts->menu_order->PlaceHolder ?>" value="<?php echo $wp_posts->menu_order->EditValue ?>"<?php echo $wp_posts->menu_order->EditAttributes() ?>>
</span>
<?php echo $wp_posts->menu_order->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_type->Visible) { // post_type ?>
	<tr id="r_post_type">
		<td><span id="elh_wp_posts_post_type"><?php echo $wp_posts->post_type->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_type->CellAttributes() ?>>
<span id="el_wp_posts_post_type" class="control-group">
<input type="text" data-field="x_post_type" name="x_post_type" id="x_post_type" size="30" maxlength="20" placeholder="<?php echo $wp_posts->post_type->PlaceHolder ?>" value="<?php echo $wp_posts->post_type->EditValue ?>"<?php echo $wp_posts->post_type->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_type->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->post_mime_type->Visible) { // post_mime_type ?>
	<tr id="r_post_mime_type">
		<td><span id="elh_wp_posts_post_mime_type"><?php echo $wp_posts->post_mime_type->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->post_mime_type->CellAttributes() ?>>
<span id="el_wp_posts_post_mime_type" class="control-group">
<input type="text" data-field="x_post_mime_type" name="x_post_mime_type" id="x_post_mime_type" size="30" maxlength="100" placeholder="<?php echo $wp_posts->post_mime_type->PlaceHolder ?>" value="<?php echo $wp_posts->post_mime_type->EditValue ?>"<?php echo $wp_posts->post_mime_type->EditAttributes() ?>>
</span>
<?php echo $wp_posts->post_mime_type->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($wp_posts->comment_count->Visible) { // comment_count ?>
	<tr id="r_comment_count">
		<td><span id="elh_wp_posts_comment_count"><?php echo $wp_posts->comment_count->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></span></td>
		<td<?php echo $wp_posts->comment_count->CellAttributes() ?>>
<span id="el_wp_posts_comment_count" class="control-group">
<input type="text" data-field="x_comment_count" name="x_comment_count" id="x_comment_count" size="30" placeholder="<?php echo $wp_posts->comment_count->PlaceHolder ?>" value="<?php echo $wp_posts->comment_count->EditValue ?>"<?php echo $wp_posts->comment_count->EditAttributes() ?>>
</span>
<?php echo $wp_posts->comment_count->CustomMsg ?></td>
	</tr>
<?php } ?>
</table>
</td></tr></table>
<button class="btn btn-primary ewButton" name="btnAction" id="btnAction" type="submit"><?php echo $Language->Phrase("EditBtn") ?></button>
</form>
<script type="text/javascript">
fwp_postsedit.Init();
<?php if (EW_MOBILE_REFLOW && ew_IsMobile()) { ?>
ew_Reflow();
<?php } ?>
</script>
<?php
$wp_posts_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$wp_posts_edit->Page_Terminate();
?>
